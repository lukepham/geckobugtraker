package es.kazbeel.geckobugtracker.web.controller.admin;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.model.Priority;
import es.kazbeel.geckobugtracker.service.IssueService;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class PriorityControllerTest {
    
    @Mock
    private IssueService issueService;
    
    @InjectMocks
    private PriorityController priorityController;
    
    private MockMvc mockMvc;
    
    
    @Before
    public void setup () {
    
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(priorityController).setViewResolvers(viewResolver).build();
    }
    
    @Test
    public void testViewPrioritys () throws Exception {
    
        mockMvc.perform(get("/admin/priorities/ViewPriorities"))
               .andExpect(view().name("/admin/priorities/ViewPriorities"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("priorities"));
        
        verify(issueService).getAllPriorities();
    }
    
    @Test
    public void testCreatePriorityGet () throws Exception {
    
        mockMvc.perform(get("/admin/priorities/CreatePriority"))
               .andExpect(view().name("/admin/priorities/CreatePriority"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("priority"));
    }
    
    @Test
    public void testCreatePriorityPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/priorities/CreatePriority").param("name", "not_empty"))
               .andExpect(redirectedUrl("/admin/priorities/ViewPriorities"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("priority"));
        
        verify(issueService).savePriority(any(Priority.class));
    }
    
    @Test
    public void testCreatePriorityPost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/priorities/CreatePriority").param("name", ""))
               .andExpect(view().name("/admin/priorities/CreatePriority"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("priority"));
        
        verify(issueService, never()).savePriority(any(Priority.class));
    }
    
    @Test
    public void testCreatePriorityPost_throwsException () throws Exception {
    
        doThrow(new DataIntegrityViolationException("Duplicate entry")).when(issueService)
                                                                       .savePriority(any(Priority.class));
        
        mockMvc.perform(post("/admin/priorities/CreatePriority").param("name", "duplicate_name"))
               .andExpect(view().name("/admin/priorities/CreatePriority"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("priority"));
        
        verify(issueService).savePriority(any(Priority.class));
    }
    
    @Test
    public void testEditPriorityGet_allOK () throws Exception {
    
        when(issueService.getPriorityById(anyInt())).thenReturn(new Priority());
        
        mockMvc.perform(get("/admin/priorities/EditPriority").param("id", "0"))
               .andExpect(view().name("/admin/priorities/EditPriority"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("priority"));
        
        verify(issueService).getPriorityById(anyInt());
    }
    
    @Test
    public void testEditPriorityPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/priorities/EditPriority").param("id", "0").param("name", "not_empty"))
               .andExpect(redirectedUrl("/admin/priorities/ViewPriorities"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("priority"));
        
        verify(issueService).savePriority(any(Priority.class));
    }
    
    @Test
    public void testEditPriorityPost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/priorities/EditPriority").param("id", "0").param("name", ""))
               .andExpect(view().name("/admin/priorities/EditPriority"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("priority"));
        
        verify(issueService, never()).savePriority(any(Priority.class));
    }
    
    @Test
    public void testEditPriorityPost_throwsException () throws Exception {
    
        doThrow(new DataIntegrityViolationException("Duplicate entry")).when(issueService)
                                                                       .savePriority(any(Priority.class));
        
        mockMvc.perform(post("/admin/priorities/EditPriority").param("id", "0").param("name", "duplicate_name"))
               .andExpect(view().name("/admin/priorities/EditPriority"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("priority"));
        
        verify(issueService).savePriority(any(Priority.class));
    }
    
    @Test
    public void testDeletePriority_allOK () throws Exception {
    
        mockMvc.perform(get("/admin/priorities/DeletePriority").param("id", "0"))
               .andExpect(redirectedUrl("/admin/priorities/ViewPriorities"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgSuccess"));
        
        verify(issueService).getPriorityById(anyInt());
        verify(issueService).deletePriority(any(Priority.class));
    }
    
    @Test
    public void testDeletePriority_entityDoesNotExist () throws Exception {
    
        doThrow(new IllegalArgumentException()).when(issueService).deletePriority(any(Priority.class));
        
        mockMvc.perform(get("/admin/priorities/DeletePriority").param("id", "0"))
               .andExpect(redirectedUrl("/admin/priorities/ViewPriorities"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(issueService).getPriorityById(anyInt());
        verify(issueService).deletePriority(any(Priority.class));
    }
    
    @Test
    public void testDeletePriority_throwsDataIntegrityViolation () throws Exception {
    
        doThrow(new DataIntegrityViolationException("")).when(issueService)
                                                        .deletePriority(any(Priority.class));
        
        mockMvc.perform(get("/admin/priorities/DeletePriority").param("id", "0"))
               .andExpect(redirectedUrl("/admin/priorities/ViewPriorities"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(issueService).getPriorityById(anyInt());
        verify(issueService).deletePriority(any(Priority.class));
    }
    
    @Test
    public void testSetPriorityOrder_moveUp () throws Exception {
    
        when(issueService.getPriorityById(anyInt())).thenReturn(new Priority());
        
        mockMvc.perform(get("/admin/priorities/SortPriority").param("id", "0").param("move", "up"))
               .andExpect(redirectedUrl("/admin/priorities/ViewPriorities"))
               .andExpect(flash().attributeCount(0));
        
        verify(issueService).getPriorityById(anyInt());
        verify(issueService).setHigherOrderForPriority(any(Priority.class));
        verify(issueService, never()).setLowerOrderForPriority(any(Priority.class));
    }

    @Test
    public void testSetPriorityOrder_moveDown () throws Exception {
    
        when(issueService.getPriorityById(anyInt())).thenReturn(new Priority());

        mockMvc.perform(get("/admin/priorities/SortPriority").param("id", "0").param("move", "down"))
               .andExpect(redirectedUrl("/admin/priorities/ViewPriorities"))
               .andExpect(flash().attributeCount(0));
        
        verify(issueService).getPriorityById(anyInt());
        verify(issueService).setLowerOrderForPriority(any(Priority.class));
        verify(issueService, never()).setHigherOrderForPriority(any(Priority.class));
    }

    @Test
    public void testSetPriorityOrder_invalidId () throws Exception {
    
        doThrow(new IllegalArgumentException()).when(issueService).setHigherOrderForPriority(any(Priority.class));
        
        mockMvc.perform(get("/admin/priorities/SortPriority").param("id", "0").param("move", "up"))
               .andExpect(redirectedUrl("/admin/priorities/ViewPriorities"))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(issueService).getPriorityById(anyInt());
        verify(issueService, never()).setLowerOrderForPriority(any(Priority.class));
        verify(issueService, never()).setHigherOrderForPriority(any(Priority.class));
    }

    @Test
    public void testSetPriorityOrder_invalidMoveAction () throws Exception {
    
        when(issueService.getPriorityById(anyInt())).thenReturn(new Priority());
        
        mockMvc.perform(get("/admin/priorities/SortPriority").param("id", "0").param("move", "invalid"))
               .andExpect(redirectedUrl("/admin/priorities/ViewPriorities"))
               .andExpect(flash().attributeCount(0));
        
        verify(issueService).getPriorityById(anyInt());
        verify(issueService, never()).setLowerOrderForPriority(any(Priority.class));
        verify(issueService, never()).setHigherOrderForPriority(any(Priority.class));
    }
}
