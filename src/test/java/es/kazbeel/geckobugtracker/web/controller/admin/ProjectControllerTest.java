package es.kazbeel.geckobugtracker.web.controller.admin;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.builders.SectionBuilder;
import es.kazbeel.geckobugtracker.model.Project;
import es.kazbeel.geckobugtracker.model.Section;
import es.kazbeel.geckobugtracker.service.ProjectService;
import es.kazbeel.geckobugtracker.service.UserGroupService;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class ProjectControllerTest {
    
    @Mock
    private ProjectService projectService;
    
    @Mock
    private UserGroupService userGroupService;
    
    @InjectMocks
    private ProjectController projectController;
    
    private MockMvc mockMvc;
    
    
    @Before
    public void setup () {
    
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(projectController).setViewResolvers(viewResolver).build();
    }
    
    private Collection<Section> generateSectionCollection () {
    
        Collection<Section> sections = new ArrayList<Section>(0);
        
        sections.add(SectionBuilder.section().withId(1L).withName("section_name_1").build());
        
        return sections;
    }
    
    @Test
    public void testViewProjects_sectionNotSelected () throws Exception {
    
        when(projectService.getAllSections()).thenReturn(generateSectionCollection());
        
        mockMvc.perform(get("/admin/projects/ViewProjects"))
               .andExpect(view().name("/admin/projects/ViewProjects"))
               .andExpect(model().size(4))
               .andExpect(model().attributeExists("sectionForm"))
               .andExpect(model().attributeExists("sections"))
               .andExpect(model().attributeExists("projects"));
        // Since <section> attribute is null, it's removed by Spring
        
        mockMvc.perform(post("/admin/projects/ViewProjects"))
               .andExpect(view().name("/admin/projects/ViewProjects"))
               .andExpect(model().size(4))
               .andExpect(model().attributeExists("sectionForm"))
               .andExpect(model().attributeExists("sections"))
               .andExpect(model().attributeExists("projects"));
        // Since <section> attribute is null, it's removed by Spring
        
        verify(projectService, times(2)).getAllSections();
        verify(projectService, times(2)).getAllProjects();
    }
    
    @Test
    public void testViewProjects_sectionSelected () throws Exception {
    
        when(projectService.getAllSections()).thenReturn(generateSectionCollection());
        
        mockMvc.perform(get("/admin/projects/ViewProjects").param("section.id", "1").param("section.name",
                                                                                           "section_name_1"))
               .andExpect(view().name("/admin/projects/ViewProjects"))
               .andExpect(model().size(4))
               .andExpect(model().attributeExists("sectionForm"))
               .andExpect(model().attributeExists("sections"))
               .andExpect(model().attributeExists("projects"))
               .andExpect(model().attributeExists("section"));
        
        mockMvc.perform(post("/admin/projects/ViewProjects").param("section.id", "1").param("section.name",
                                                                                            "section_name_1"))
               .andExpect(view().name("/admin/projects/ViewProjects"))
               .andExpect(model().size(4))
               .andExpect(model().attributeExists("sectionForm"))
               .andExpect(model().attributeExists("sections"))
               .andExpect(model().attributeExists("projects"))
               .andExpect(model().attributeExists("section"));
        
        verify(projectService, times(2)).getAllSections();
        verify(projectService, times(2)).getProjectsBySection(any(Section.class));
    }
    
    @Test
    public void testViewProjects_doesNotExistAnySection () throws Exception {
    
        when(projectService.getAllSections()).thenReturn(new ArrayList<Section>());
        
        mockMvc.perform(get("/admin/projects/ViewProjects"))
               .andExpect(redirectedUrl("/admin/sections/ViewSections"))
               .andExpect(model().size(0));
        
        mockMvc.perform(post("/admin/projects/ViewProjects"))
               .andExpect(redirectedUrl("/admin/sections/ViewSections"))
               .andExpect(model().size(0));
        
        verify(projectService, times(2)).getAllSections();
    }
    
    @Test
    public void testCreateProjectForm_sectionNotSelected () throws Exception {
    
        mockMvc.perform(post("/admin/projects/CreateProjectForm").param("sectionId", "-1"))
               .andExpect(view().name("/admin/projects/CreateProject"))
               .andExpect(model().size(4))
               .andExpect(model().attributeExists("sectionId"))
               .andExpect(model().attributeExists("sections"))
               .andExpect(model().attributeExists("users"))
               .andExpect(model().attributeExists("project"));
        
        verify(projectService).getAllSections();
        verify(userGroupService).getAllEnabledUsers();
    }
    
    @Test
    public void testCreateProjectForm_sectionSelected () throws Exception {
    
        Section section = SectionBuilder.section().withId(1L).withName("section_name_1").build();
        
        when(projectService.getSectionById(anyLong())).thenReturn(section);
        
        mockMvc.perform(post("/admin/projects/CreateProjectForm").param("sectionId", "1"))
               .andExpect(view().name("/admin/projects/CreateProject"))
               .andExpect(model().size(4))
               .andExpect(model().attributeExists("sectionId"))
               .andExpect(model().attributeExists("section"))
               .andExpect(model().attributeExists("users"))
               .andExpect(model().attributeExists("project"));
        
        verify(projectService).getSectionById(anyLong());
        verify(userGroupService).getAllEnabledUsers();
    }
    
    @Test
    public void testCreateProjectPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/projects/CreateProject").param("id", "1")
                                                             .param("name", "project_name")
                                                             .param("section.id", "2")
                                                             .param("devLeader.id", "3")
                                                             .param("qaLeader.id", "4"))
               .andExpect(redirectedUrl("/admin/projects/DetailsProject?id=1"))
               .andExpect(model().hasNoErrors())
               .andExpect(model().size(1))
               .andExpect(model().attribute("id", "1"));
        
        verify(projectService).saveProjectWithChildComponent(any(Project.class));
    }
    
    @Test
    public void testCreateProjectPost_withBindingResultError () throws Exception {
    
        mockMvc.perform(post("/admin/projects/CreateProject"))
               .andExpect(view().name("/admin/projects/CreateProject"))
               .andExpect(model().hasErrors());
        
        verify(projectService, never()).saveProjectWithChildComponent(any(Project.class));
    }
    
    @Test
    public void testCreateProjectPost_throwsDataIntegrityViolation () throws Exception {
    
        doThrow(DataIntegrityViolationException.class).when(projectService)
                                                      .saveProjectWithChildComponent(any(Project.class));
        
        mockMvc.perform(post("/admin/projects/CreateProject").param("id", "1")
                                                             .param("name", "project_name")
                                                             .param("section.id", "2")
                                                             .param("devLeader.id", "3")
                                                             .param("qaLeader.id", "4"))
               .andExpect(view().name("/admin/projects/CreateProject"))
               .andExpect(model().hasNoErrors());
        
        verify(projectService).saveProjectWithChildComponent(any(Project.class));
    }
    
    @Test
    public void testEditProjectGet () throws Exception {
    
        when(projectService.getProjectById(anyLong())).thenReturn(new Project());
        
        mockMvc.perform(get("/admin/projects/EditProject").param("id", "1"))
               .andExpect(view().name("/admin/projects/EditProject"))
               .andExpect(model().size(3))
               .andExpect(model().attributeExists("users"))
               .andExpect(model().attributeExists("sections"))
               .andExpect(model().attributeExists("project"));
        
        verify(userGroupService).getAllEnabledUsers();
        verify(projectService).getAllSections();
        verify(projectService).getProjectById(anyLong());
    }
    
    @Test
    public void testEditProjectPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/projects/EditProject").param("id", "1")
                                                           .param("name", "project_name")
                                                           .param("section.id", "2")
                                                           .param("devLeader.id", "3")
                                                           .param("qaLeader.id", "4"))
               .andExpect(redirectedUrl("/admin/projects/DetailsProject?id=1"))
               .andExpect(model().hasNoErrors())
               .andExpect(model().size(1))
               .andExpect(model().attribute("id", "1"));
        
        verify(projectService).saveProject(any(Project.class));
    }
    
    @Test
    public void testEditProjectPost_withBindingResultError () throws Exception {
    
        mockMvc.perform(post("/admin/projects/EditProject").param("id", "1"))
               .andExpect(view().name("/admin/projects/EditProject"))
               .andExpect(model().hasErrors());
        
        verify(projectService, never()).saveProject(any(Project.class));
    }
    
    @Test
    public void testEditProjectPost_throwsDataIntegrityViolation () throws Exception {
    
        doThrow(DataIntegrityViolationException.class).when(projectService).saveProject(any(Project.class));
        
        mockMvc.perform(post("/admin/projects/EditProject").param("id", "1")
                                                           .param("name", "project_name")
                                                           .param("section.id", "2")
                                                           .param("devLeader.id", "3")
                                                           .param("qaLeader.id", "4"))
               .andExpect(view().name("/admin/projects/EditProject"))
               .andExpect(model().hasNoErrors());
        
        verify(projectService).saveProject(any(Project.class));
    }
    
    @Test
    public void testDeleteProject_allOK () throws Exception {
    
        mockMvc.perform(get("/admin/projects/DeleteProject").param("id", "0"))
               .andExpect(redirectedUrl("/admin/projects/ViewProjects"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgSuccess"));
        
        verify(projectService).getProjectById(anyLong());
        verify(projectService).deleteProject(any(Project.class));
    }
    
    @Test
    public void testDeleteProject_entityDoesNotExist () throws Exception {
    
        doThrow(new IllegalArgumentException()).when(projectService).deleteProject(any(Project.class));
        
        mockMvc.perform(get("/admin/projects/DeleteProject").param("id", "0"))
               .andExpect(redirectedUrl("/admin/projects/ViewProjects"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(projectService).getProjectById(anyLong());
        verify(projectService).deleteProject(any(Project.class));
    }
    
    @Test
    public void testDeleteProject_throwsDataIntegrityViolation () throws Exception {
    
        doThrow(DataIntegrityViolationException.class).when(projectService).deleteProject(any(Project.class));
        
        mockMvc.perform(get("/admin/projects/DeleteProject").param("id", "0"))
               .andExpect(redirectedUrl("/admin/projects/ViewProjects"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(projectService).getProjectById(anyLong());
        verify(projectService).deleteProject(any(Project.class));
    }
    
    @Test
    public void testDetailsProject_allOK () throws Exception {
    
        when(projectService.getProjectByIdWithSharedComponentsAndVersions(anyLong())).thenReturn(new Project());
        
        mockMvc.perform(get("/admin/projects/DetailsProject").param("id", "1"))
               .andExpect(view().name("/admin/projects/DetailsProject"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("project"));
        
        verify(projectService).getProjectByIdWithSharedComponentsAndVersions(anyLong());
    }
    
    @Test
    public void testDetailsProject_entityDoesNotExist () throws Exception {
    
        when(projectService.getProjectByIdWithSharedComponentsAndVersions(anyLong())).thenReturn(null);
        
        mockMvc.perform(get("/admin/projects/DetailsProject").param("id", "1"))
               .andExpect(status().isNotFound());
        
        verify(projectService).getProjectByIdWithSharedComponentsAndVersions(anyLong());
    }
}
