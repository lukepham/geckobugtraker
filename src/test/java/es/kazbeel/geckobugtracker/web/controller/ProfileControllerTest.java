package es.kazbeel.geckobugtracker.web.controller;


import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.service.UserGroupService;
import es.kazbeel.geckobugtracker.web.secure.CurrentUserDetails;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class ProfileControllerTest {
    
    @Mock
    private UserGroupService userGroupService;
    
    @InjectMocks
    private ProfileController profileController;
    
    private MockMvc mockMvc;
    
    private User admin = new User();
    
    
    @Before
    public void setup () {
    
        admin.setLoginName("admin");
        admin.setFirstName("first_name");
        admin.setLastName("last_name");
        admin.setEmail("email@email.com");
        admin.setPassword("admin");
        
        CurrentUserDetails loggedUser = new CurrentUserDetails(admin.getLoginName(),
                                                               admin.getPassword(),
                                                               AuthorityUtils.createAuthorityList("ROLE_ADMIN"),
                                                               admin);
        Authentication auth = new UsernamePasswordAuthenticationToken(loggedUser, null);
        SecurityContextHolder.getContext().setAuthentication(auth);
        
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(profileController).setViewResolvers(viewResolver).build();
    }
    
    @Test
    public void testViewProfileGet () throws Exception {
    
        mockMvc.perform(get("/profile/ViewProfile"))
               .andExpect(view().name("/profile/ViewProfile"))
               .andExpect(model().size(1))
               .andExpect(model().attribute("loggedUser", is(admin)));
    }
    
    @Test
    public void testEditProfileGet () throws Exception {
    
        mockMvc.perform(get("/profile/EditProfile"))
               .andExpect(view().name("/profile/EditProfile"))
               .andExpect(model().size(1))
               .andExpect(model().attribute("loggedUser", is(admin)));
    }
    
    @Test
    public void testEditProfilePost_allOK () throws Exception {
    
        mockMvc.perform(post("/profile/EditProfile").sessionAttr("loggedUser", admin))
               .andExpect(redirectedUrl("/profile/ViewProfile"));
        
        verify(userGroupService).saveUser(admin);
    }
    
    @Test
    public void testEditProfilePost_userIsInvalid () throws Exception {
    
        admin.setFirstName(RandomStringUtils.randomAlphabetic(65));
        
        mockMvc.perform(post("/profile/EditProfile").sessionAttr("loggedUser", admin))
               .andExpect(view().name("/profile/EditProfile"));
        
        verify(userGroupService, never()).saveUser(admin);
    }
}
