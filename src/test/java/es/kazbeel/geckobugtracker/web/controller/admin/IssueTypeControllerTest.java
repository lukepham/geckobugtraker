package es.kazbeel.geckobugtracker.web.controller.admin;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.model.IssueType;
import es.kazbeel.geckobugtracker.service.IssueService;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class IssueTypeControllerTest {
    
    @Mock
    private IssueService issueService;
    
    @InjectMocks
    private IssueTypeController issueTypeController;
    
    private MockMvc mockMvc;
    
    
    @Before
    public void setup () {
    
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(issueTypeController).setViewResolvers(viewResolver).build();
    }
    
    @Test
    public void testViewIssueTypes () throws Exception {
    
        mockMvc.perform(get("/admin/issuetypes/ViewIssueTypes"))
               .andExpect(view().name("/admin/issuetypes/ViewIssueTypes"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueTypes"));
        
        verify(issueService).getAllIssueTypes();
    }
    
    @Test
    public void testCreateIssueTypeGet () throws Exception {
    
        mockMvc.perform(get("/admin/issuetypes/CreateIssueType"))
               .andExpect(view().name("/admin/issuetypes/CreateIssueType"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueType"));
    }
    
    @Test
    public void testCreateIssueTypePost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/issuetypes/CreateIssueType").param("name", "not_empty"))
               .andExpect(redirectedUrl("/admin/issuetypes/ViewIssueTypes"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueType"));
        
        verify(issueService).saveIssueType(any(IssueType.class));
    }
    
    @Test
    public void testCreateIssueTypePost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/issuetypes/CreateIssueType").param("name", ""))
               .andExpect(view().name("/admin/issuetypes/CreateIssueType"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueType"));
        
        verify(issueService, never()).saveIssueType(any(IssueType.class));
    }
    
    @Test
    public void testCreateIssueTypePost_throwsException () throws Exception {
    
        doThrow(new DataIntegrityViolationException("Duplicate entry")).when(issueService)
                                                                       .saveIssueType(any(IssueType.class));
        
        mockMvc.perform(post("/admin/issuetypes/CreateIssueType").param("name", "duplicate_name"))
               .andExpect(view().name("/admin/issuetypes/CreateIssueType"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueType"));
        
        verify(issueService).saveIssueType(any(IssueType.class));
    }
    
    @Test
    public void testEditIssueTypeGet_allOK () throws Exception {
    
        when(issueService.getIssueTypeById(anyInt())).thenReturn(new IssueType());
        
        mockMvc.perform(get("/admin/issuetypes/EditIssueType").param("id", "0"))
               .andExpect(view().name("/admin/issuetypes/EditIssueType"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueType"));
        
        verify(issueService).getIssueTypeById(anyInt());
    }
    
    @Test
    public void testEditIssueTypePost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/issuetypes/EditIssueType").param("id", "0").param("name", "not_empty"))
               .andExpect(redirectedUrl("/admin/issuetypes/ViewIssueTypes"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueType"));
        
        verify(issueService).saveIssueType(any(IssueType.class));
    }
    
    @Test
    public void testEditIssueTypePost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/issuetypes/EditIssueType").param("id", "0").param("name", ""))
               .andExpect(view().name("/admin/issuetypes/EditIssueType"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueType"));
        
        verify(issueService, never()).saveIssueType(any(IssueType.class));
    }
    
    @Test
    public void testEditIssueTypePost_throwsException () throws Exception {
    
        doThrow(new DataIntegrityViolationException("Duplicate entry")).when(issueService)
                                                                       .saveIssueType(any(IssueType.class));
        
        mockMvc.perform(post("/admin/issuetypes/EditIssueType").param("id", "0").param("name", "duplicate_name"))
               .andExpect(view().name("/admin/issuetypes/EditIssueType"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueType"));
        
        verify(issueService).saveIssueType(any(IssueType.class));
    }
    
    @Test
    public void testDeleteIssueType_allOK () throws Exception {
    
        mockMvc.perform(get("/admin/issuetypes/DeleteIssueType").param("id", "0"))
               .andExpect(redirectedUrl("/admin/issuetypes/ViewIssueTypes"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgSuccess"));
        
        verify(issueService).getIssueTypeById(anyInt());
        verify(issueService).deleteIssueType(any(IssueType.class));
    }
    
    @Test
    public void testDeleteIssueType_entityDoesNotExist () throws Exception {
    
        doThrow(new IllegalArgumentException()).when(issueService).deleteIssueType(any(IssueType.class));
        
        mockMvc.perform(get("/admin/issuetypes/DeleteIssueType").param("id", "0"))
               .andExpect(redirectedUrl("/admin/issuetypes/ViewIssueTypes"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(issueService).getIssueTypeById(anyInt());
        verify(issueService).deleteIssueType(any(IssueType.class));
    }
    
    @Test
    public void testDeleteIssueType_throwsDataIntegrityViolation () throws Exception {
    
        doThrow(new DataIntegrityViolationException("")).when(issueService)
                                                        .deleteIssueType(any(IssueType.class));
        
        mockMvc.perform(get("/admin/issuetypes/DeleteIssueType").param("id", "0"))
               .andExpect(redirectedUrl("/admin/issuetypes/ViewIssueTypes"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(issueService).getIssueTypeById(anyInt());
        verify(issueService).deleteIssueType(any(IssueType.class));
    }
}
