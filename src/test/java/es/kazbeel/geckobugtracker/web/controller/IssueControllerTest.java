package es.kazbeel.geckobugtracker.web.controller;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.model.Issue;
import es.kazbeel.geckobugtracker.model.Resolution;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.service.IssueService;
import es.kazbeel.geckobugtracker.service.UserGroupService;
import es.kazbeel.geckobugtracker.support.Page;
import es.kazbeel.geckobugtracker.support.PageRequest;
import es.kazbeel.geckobugtracker.web.controller.IssueController;
import es.kazbeel.geckobugtracker.web.secure.CurrentUserDetails;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class IssueControllerTest {
    
    @Mock
    private IssueService issueService;
    
    @Mock
    private UserGroupService userGroupService;
    
    @InjectMocks
    private IssueController issueController;
    
    private MockMvc mockMvc;
    
    private User admin = new User();
    
    
    @Before
    public void setup () {
    
        CurrentUserDetails loggedUser = new CurrentUserDetails("admin",
                                                               "admin",
                                                               AuthorityUtils.createAuthorityList("ROLE_ADMIN"),
                                                               admin);
        Authentication auth = new UsernamePasswordAuthenticationToken(loggedUser, null);
        SecurityContextHolder.getContext().setAuthentication(auth);
        
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(issueController).setViewResolvers(viewResolver).build();
    }
    
    @Test
    public void testAllIssuesGet_PageNumberOK () throws Exception {
    
        @SuppressWarnings("unchecked")
        Page<Issue> page = mock(Page.class);
        
        when(issueService.getAllIssues(any(PageRequest.class))).thenReturn(page);
        when(page.getTotalPages()).thenReturn(0);
        
        mockMvc.perform(get("/issues/AllIssues").param("page", "0"))
               .andExpect(view().name("issues/AllIssues"))
               .andExpect(model().size(5))
               .andExpect(model().attributeExists("loggedUser"))
               .andExpect(model().attributeExists("issues"))
               .andExpect(model().attributeExists("totalPages"))
               .andExpect(model().attributeExists("isFirst"))
               .andExpect(model().attributeExists("isLast"));
    }
    
    @Test
    public void testAllIssuesGet_PageNumberNOK () throws Exception {
    
        @SuppressWarnings("unchecked")
        Page<Issue> page = mock(Page.class);
        
        when(issueService.getAllIssues(any(PageRequest.class))).thenReturn(page);
        when(page.getTotalPages()).thenReturn(0);
        
        mockMvc.perform(get("/issues/AllIssues").param("page", "1"))
               .andExpect(redirectedUrl("AllIssues"))
               .andExpect(model().size(0));
    }
    
    @Test
    public void testAssignedToMeAndUnresolvedIssuesGet_PageNumberOK () throws Exception {
    
        @SuppressWarnings("unchecked")
        Page<Issue> page = mock(Page.class);
        
        when(issueService.getIssuesByAssigneeAndResolution(eq(admin), any(Resolution.class), any(PageRequest.class))).thenReturn(page);
        when(page.getTotalPages()).thenReturn(0);
        
        mockMvc.perform(get("/issues/AssignedToMe").param("page", "0"))
               .andExpect(view().name("issues/AssignedToMe"))
               .andExpect(model().size(5))
               .andExpect(model().attributeExists("loggedUser"))
               .andExpect(model().attributeExists("issues"))
               .andExpect(model().attributeExists("totalPages"))
               .andExpect(model().attributeExists("isFirst"))
               .andExpect(model().attributeExists("isLast"));
    }
    
    @Test
    public void testAssignedToMeAndUnresolvedIssuesGet_PageNumberNOK () throws Exception {
    
        @SuppressWarnings("unchecked")
        Page<Issue> page = mock(Page.class);
        
        when(issueService.getIssuesByAssigneeAndResolution(eq(admin), any(Resolution.class), any(PageRequest.class))).thenReturn(page);
        when(page.getTotalPages()).thenReturn(0);
        when(page.getContent()).thenReturn(new ArrayList<Issue>());
        when(page.isFirst()).thenReturn(false);
        when(page.isLast()).thenReturn(false);
        
        mockMvc.perform(get("/issues/AssignedToMe").param("page", "1"))
               .andExpect(redirectedUrl("AssignedToMe"))
               .andExpect(model().size(0));
    }
    
    @Test
    public void testReportedByMeGet_PageNumberOK () throws Exception {
    
        @SuppressWarnings("unchecked")
        Page<Issue> page = mock(Page.class);
        
        when(issueService.getIssuesByReporter(eq(admin), any(PageRequest.class))).thenReturn(page);
        when(page.getTotalPages()).thenReturn(0);
        
        mockMvc.perform(get("/issues/ReportedByMe").param("page", "0"))
               .andExpect(view().name("issues/ReportedByMe"))
               .andExpect(model().size(5))
               .andExpect(model().attributeExists("loggedUser"))
               .andExpect(model().attributeExists("issues"))
               .andExpect(model().attributeExists("totalPages"))
               .andExpect(model().attributeExists("isFirst"))
               .andExpect(model().attributeExists("isLast"));
    }
    
    @Test
    public void testReportedByMeGet_PageNumberNOK () throws Exception {
    
        @SuppressWarnings("unchecked")
        Page<Issue> page = mock(Page.class);
        
        when(issueService.getIssuesByReporter(eq(admin), any(PageRequest.class))).thenReturn(page);
        when(page.getTotalPages()).thenReturn(0);
        when(page.getContent()).thenReturn(new ArrayList<Issue>());
        when(page.isFirst()).thenReturn(false);
        when(page.isLast()).thenReturn(false);
        
        mockMvc.perform(get("/issues/ReportedByMe").param("page", "1"))
               .andExpect(redirectedUrl("ReportedByMe"))
               .andExpect(model().size(0));
    }
    
    @Test
    public void testVotedByMeGet_PageNumberOK () throws Exception {
    
        @SuppressWarnings("unchecked")
        Page<Issue> page = mock(Page.class);
        
        when(issueService.getIssuesVotedByUser(eq(admin), any(PageRequest.class))).thenReturn(page);
        when(page.getTotalPages()).thenReturn(0);
        
        mockMvc.perform(get("/issues/VotedByMe").param("page", "0"))
               .andExpect(view().name("issues/VotedByMe"))
               .andExpect(model().size(5))
               .andExpect(model().attributeExists("loggedUser"))
               .andExpect(model().attributeExists("issues"))
               .andExpect(model().attributeExists("totalPages"))
               .andExpect(model().attributeExists("isFirst"))
               .andExpect(model().attributeExists("isLast"));
    }
    
    @Test
    public void testVotedByMeGet_PageNumberNOK () throws Exception {
    
        @SuppressWarnings("unchecked")
        Page<Issue> page = mock(Page.class);
        
        when(issueService.getIssuesVotedByUser(eq(admin), any(PageRequest.class))).thenReturn(page);
        when(page.getTotalPages()).thenReturn(0);
        when(page.getContent()).thenReturn(new ArrayList<Issue>());
        when(page.isFirst()).thenReturn(false);
        when(page.isLast()).thenReturn(false);
        
        mockMvc.perform(get("/issues/VotedByMe").param("page", "1"))
               .andExpect(redirectedUrl("VotedByMe"))
               .andExpect(model().size(0));
    }
    
    @Test
    public void testWatchedByMeGet_PageNumberOK () throws Exception {
    
        @SuppressWarnings("unchecked")
        Page<Issue> page = mock(Page.class);
        
        when(issueService.getIssuesWatchedByUser(eq(admin), any(PageRequest.class))).thenReturn(page);
        when(page.getTotalPages()).thenReturn(0);
        
        mockMvc.perform(get("/issues/WatchedByMe").param("page", "0"))
               .andExpect(view().name("issues/WatchedByMe"))
               .andExpect(model().size(5))
               .andExpect(model().attributeExists("loggedUser"))
               .andExpect(model().attributeExists("issues"))
               .andExpect(model().attributeExists("totalPages"))
               .andExpect(model().attributeExists("isFirst"))
               .andExpect(model().attributeExists("isLast"));
    }
    
    @Test
    public void testWatchedByMeGet_PageNumberNOK () throws Exception {
    
        @SuppressWarnings("unchecked")
        Page<Issue> page = mock(Page.class);
        
        when(issueService.getIssuesWatchedByUser(eq(admin), any(PageRequest.class))).thenReturn(page);
        when(page.getTotalPages()).thenReturn(0);
        when(page.getContent()).thenReturn(new ArrayList<Issue>());
        when(page.isFirst()).thenReturn(false);
        when(page.isLast()).thenReturn(false);
        
        mockMvc.perform(get("/issues/WatchedByMe").param("page", "1"))
               .andExpect(redirectedUrl("WatchedByMe"))
               .andExpect(model().size(0));
    }
}
