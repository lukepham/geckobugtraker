package es.kazbeel.geckobugtracker.web.controller.admin;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.builders.ProjectBuilder;
import es.kazbeel.geckobugtracker.builders.ProjectVersionBuilder;
import es.kazbeel.geckobugtracker.model.Project;
import es.kazbeel.geckobugtracker.model.ProjectVersion;
import es.kazbeel.geckobugtracker.service.ProjectService;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class ProjectVersionControllerTest {
    
    @Mock
    private ProjectService projectService;
    
    @InjectMocks
    private ProjectVersionController projectVersionController;
    
    private MockMvc mockMvc;
    
    
    @Before
    public void setup () {
    
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(projectVersionController).setViewResolvers(viewResolver).build();
    }
    
    @Test
    public void testCreateProjectVersionForm () throws Exception {
    
        when(projectService.getProjectById(anyLong())).thenReturn(new Project());
        
        mockMvc.perform(post("/admin/projectversions/CreateProjectVersionForm").param("projectId", "1"))
               .andExpect(view().name("/admin/projectversions/CreateProjectVersion"))
               .andExpect(model().size(3))
               .andExpect(model().attributeExists("projectId"))
               .andExpect(model().attributeExists("project"))
               .andExpect(model().attributeExists("projectVersion"));
        
        verify(projectService).getProjectById(anyLong());
    }
    
    @Test
    public void testCreateProjectVersionPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/projectversions/CreateProjectVersion").param("id", "1")
                                                                           .param("name", "component_name")
                                                                           .param("devLeader.id", "2")
                                                                           .param("qaLeader.id", "3")
                                                                           .param("project.id", "4"))
               .andExpect(redirectedUrl("/admin/projects/DetailsProject?id=4"))
               .andExpect(model().hasNoErrors())
               .andExpect(model().size(1))
               .andExpect(model().attribute("id", "4"));
        
        verify(projectService).saveProjectVersion(any(ProjectVersion.class));
    }
    
    @Test
    public void testCreateProjectVersionPost_withBindingResultError () throws Exception {
    
        mockMvc.perform(post("/admin/projectversions/CreateProjectVersion"))
               .andExpect(view().name("/admin/projectversions/CreateProjectVersionForm"))
               .andExpect(model().hasErrors());
        
        verify(projectService, never()).saveProjectVersion(any(ProjectVersion.class));
    }
    
    @Test
    public void testCreateProjectVersiontPost_throwsDataIntegrityViolation () throws Exception {
    
        doThrow(DataIntegrityViolationException.class).when(projectService)
                                                      .saveProjectVersion(any(ProjectVersion.class));
        
        mockMvc.perform(post("/admin/projectversions/CreateProjectVersion").param("id", "1")
                                                                           .param("name", "component_name")
                                                                           .param("devLeader.id", "2")
                                                                           .param("qaLeader.id", "3")
                                                                           .param("project.id", "4"))
               .andExpect(view().name("/admin/projectversions/CreateProjectVersionForm"))
               .andExpect(model().hasNoErrors());
        
        verify(projectService).saveProjectVersion(any(ProjectVersion.class));
    }
    
    @Test
    public void testEditProjectVersionGet () throws Exception {
    
        when(projectService.getProjectVersionById(anyLong())).thenReturn(new ProjectVersion());
        
        mockMvc.perform(get("/admin/projectversions/EditProjectVersion").param("id", "1"))
               .andExpect(view().name("/admin/projectversions/EditProjectVersion"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("projectVersion"));
    }
    
    @Test
    public void testEditProjectVersionPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/projectversions/EditProjectVersion").param("id", "1")
                                                                         .param("name", "component_name")
                                                                         .param("devLeader.id", "2")
                                                                         .param("qaLeader.id", "3")
                                                                         .param("project.id", "4"))
               .andExpect(redirectedUrl("/admin/projectversions/DetailsProjectVersion?id=1"))
               .andExpect(model().hasNoErrors())
               .andExpect(model().size(1))
               .andExpect(model().attribute("id", "1"));
        
        verify(projectService).saveProjectVersion(any(ProjectVersion.class));
    }
    
    @Test
    public void testEditProjectVersionPost_withBindingResultError () throws Exception {
    
        mockMvc.perform(post("/admin/projectversions/EditProjectVersion").param("id", "1"))
               .andExpect(view().name("/admin/projectversions/EditProjectVersion"))
               .andExpect(model().hasErrors());
        
        verify(projectService, never()).saveProjectVersion(any(ProjectVersion.class));
    }
    
    @Test
    public void testEditProjectVersionPost_throwsDataIntegrityViolation () throws Exception {
    
        doThrow(DataIntegrityViolationException.class).when(projectService)
                                                      .saveProjectVersion(any(ProjectVersion.class));
        
        mockMvc.perform(post("/admin/projectversions/EditProjectVersion").param("id", "1")
                                                                         .param("name", "component_name")
                                                                         .param("devLeader.id", "2")
                                                                         .param("qaLeader.id", "3")
                                                                         .param("project.id", "4"))
               .andExpect(view().name("/admin/projectversions/EditProjectVersion"))
               .andExpect(model().hasNoErrors());
        
        verify(projectService).saveProjectVersion(any(ProjectVersion.class));
    }
    
    @Test
    public void testDeleteProjectVersion_allOK () throws Exception {
    
        Project dummyProject = ProjectBuilder.project().withId(2L).withName("project_name").build();
        ProjectVersion dummyProjectVersion = ProjectVersionBuilder.projectVersion()
                                                                  .withId(1L)
                                                                  .withName("project_version_name")
                                                                  .withProject(dummyProject)
                                                                  .build();
        when(projectService.getProjectVersionById(anyLong())).thenReturn(dummyProjectVersion);
        
        mockMvc.perform(get("/admin/projectversions/DeleteProjectVersion").param("id", "1"))
               .andExpect(redirectedUrl("/admin/projects/DetailsProject?id=2"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("id"))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgSuccess"));
        
        verify(projectService).getProjectVersionById(anyLong());
        verify(projectService).deleteProjectVersion(any(ProjectVersion.class));
    }
    
    @Test
    public void testDeleteProjectVersion_entityDoesNotExist () throws Exception {
    
        doThrow(IllegalArgumentException.class).when(projectService).deleteProjectVersion(any(ProjectVersion.class));
        
        mockMvc.perform(get("/admin/projectversions/DeleteProjectVersion").param("id", "1"))
               .andExpect(redirectedUrl("/admin/projects/ViewProjects"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(projectService).getProjectVersionById(anyLong());
        verify(projectService).deleteProjectVersion(any(ProjectVersion.class));
    }
    
    @Test
    public void testDeleteProjectVersion_throwsDataIntegrityViolation () throws Exception {
    
        doThrow(DataIntegrityViolationException.class).when(projectService)
                                                      .deleteProjectVersion(any(ProjectVersion.class));
        
        mockMvc.perform(get("/admin/projectversions/DeleteProjectVersion").param("id", "1"))
               .andExpect(redirectedUrl("/admin/projects/ViewProjects"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(projectService).getProjectVersionById(anyLong());
        verify(projectService).deleteProjectVersion(any(ProjectVersion.class));
    }
    
    @Test
    public void testDetailsProjectVersion_allOK () throws Exception {
    
        when(projectService.getProjectVersionById(anyLong())).thenReturn(new ProjectVersion());
        
        mockMvc.perform(get("/admin/projectversions/DetailsProjectVersion").param("id", "1"))
               .andExpect(view().name("/admin/projectversions/DetailsProjectVersion"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("projectVersion"));
        
        verify(projectService).getProjectVersionById(anyLong());
    }
    
    @Test
    public void testDetailsProjectVersion_entityDoesNotExist () throws Exception {
    
        when(projectService.getProjectVersionById(anyLong())).thenReturn(null);
        
        mockMvc.perform(get("/admin/projectversions/DetailsProjectVersion").param("id", "1"))
               .andExpect(status().isNotFound());
        
        verify(projectService).getProjectVersionById(anyLong());
    }
}
