package es.kazbeel.geckobugtracker.web.secure;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;

import es.kazbeel.geckobugtracker.model.RemembermeToken;
import es.kazbeel.geckobugtracker.service.AuthService;


@RunWith(MockitoJUnitRunner.class)
public class GeckoTokenRepositoryTest {
    
    @Mock
    private AuthService authService;
    
    @InjectMocks
    private GeckoTokenRepositoryImpl geckoTokenRepository;
    
    @Test
    public void testCreateNewToken () {
    
        PersistentRememberMeToken prmt = new PersistentRememberMeToken("user_name", "series", "token_value", new Date());
        
        geckoTokenRepository.createNewToken(prmt);
        
        verify(authService).saveToken(any(RemembermeToken.class));
    }

    @Test
    public void testUpdateToken () {
    
        Date lastUsed = new Date();
        
        geckoTokenRepository.updateToken("series", "token_value", lastUsed);
        
        verify(authService).saveToken("series", "token_value", lastUsed);
    }

    @Test
    public void testGetTokenForSeries_allOK () {
    
        when(authService.getRemembermeTokenBySeries(anyString())).thenReturn(new RemembermeToken());
        
        PersistentRememberMeToken prmt = geckoTokenRepository.getTokenForSeries("series");
        
        verify(authService).getRemembermeTokenBySeries("series");
        
        assertThat(prmt, is(not(nullValue())));
    }

    @Test
    public void testGetTokenForSeries_notFound () {
    
        when(authService.getRemembermeTokenBySeries(anyString())).thenReturn(null);
        
        PersistentRememberMeToken prmt = geckoTokenRepository.getTokenForSeries("series");
        
        verify(authService).getRemembermeTokenBySeries("series");
        
        assertThat(prmt, is(nullValue()));
    }

    @Test
    public void testRemoveUserTokens () {
    
        geckoTokenRepository.removeUserTokens("user_name");
        
        verify(authService).deleteTokensFromUsername("user_name");
    }
}
