package es.kazbeel.geckobugtracker.model;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.EnvironmentBuilder;
import es.kazbeel.geckobugtracker.builders.IssueBuilder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class EnvironmentTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("EnvironmentTest.xml")
    @Transactional
    public void testNameCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Environment env = EnvironmentBuilder.environment().withName(null).withEnabled(true).build();
        
        session.save(env);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("EnvironmentTest.xml")
    @Transactional
    public void testNameCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Environment env = EnvironmentBuilder.environment()
                                            .withName(RandomStringUtils.randomAlphabetic(256))
                                            .withEnabled(true)
                                            .build();
        
        session.save(env);
        session.flush();
        session.clear();
    }
    
    @Test(expected = org.hibernate.exception.ConstraintViolationException.class)
    @DatabaseSetup("EnvironmentTest.xml")
    @Transactional
    public void testNameIsUnique () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Environment env1 = (Environment) session.get(Environment.class, 1);
        Environment env2 = (Environment) session.get(Environment.class, 2);
        
        env1.setName(env2.getName());
        
        session.save(env1);
        session.flush();
        session.clear();
    }
    
    @Test
    @DatabaseSetup("EnvironmentTest.xml")
    @Transactional
    public void testEnvironmentWithoutIssues () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Environment env = (Environment) session.get(Environment.class, 3);
        
        assertThat(env, is(not(nullValue())));
        assertThat(env.getIssues(), is(empty()));
    }
    
    @Test
    @DatabaseSetup("EnvironmentTest.xml")
    @Transactional
    public void testEnvironmentWithOneIssue () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Environment env = (Environment) session.get(Environment.class, 2);
        
        assertThat(env, is(not(nullValue())));
        assertThat(env.getIssues(), hasSize(1));
        assertThat(env.getIssues(), contains(IssueBuilder.issue().withId(3).build()));
    }
    
    @Test
    @DatabaseSetup("EnvironmentTest.xml")
    @Transactional
    public void testEnvironmentWithMultipleIssues () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Environment env = (Environment) session.get(Environment.class, 1);
        
        assertThat(env, is(not(nullValue())));
        assertThat(env.getIssues(), hasSize(2));
        assertThat(env.getIssues(),
                   containsInAnyOrder(IssueBuilder.issue().withId(1).build(),
                                      IssueBuilder.issue().withId(3).build()));
    }
}
