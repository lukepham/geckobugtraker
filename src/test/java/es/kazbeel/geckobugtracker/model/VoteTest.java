package es.kazbeel.geckobugtracker.model;


import static org.junit.Assert.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class VoteTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test
    @DatabaseSetup("VoteTest.xml")
    @Transactional
    public void testGetVoteWithActorTypeEmpty () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Vote vote = (Vote) session.createQuery("FROM Vote vote WHERE vote.id=1").uniqueResult();
        
        assertNull(vote);
    }
    
    @Test
    @DatabaseSetup("VoteTest.xml")
    @Transactional
    public void testGetVoteWithActorTypeWrong () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Vote vote = (Vote) session.createQuery("FROM Vote WHERE id=4").uniqueResult();
        
        assertNull(vote);
    }
    
    @Test
    @DatabaseSetup("VoteTest.xml")
    @Transactional
    public void testGetVoteWithInvalidActorType () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Vote vote = (Vote) session.createQuery("FROM Vote WHERE id=5").uniqueResult();
        
        assertNull(vote);
    }
    
    @Test
    @DatabaseSetup("VoteTest.xml")
    @Transactional
    public void testGetVoteWithValidActorType () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Vote vote = (Vote) session.createQuery("FROM Vote WHERE id=2").uniqueResult();
        
        assertNotNull(vote);
        assertEquals(vote.getId(), new Integer(2));
    }
    
    @Test
    @DatabaseSetup("VoteTest.xml")
    @Transactional
    public void testCountAllVotesWithValidActorType () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Long totalVotes = (Long) session.createQuery("SELECT COUNT(*) FROM Vote").uniqueResult();
        
        assertNotNull(totalVotes);
        assertEquals(totalVotes, new Long(2));
    }
    
}
