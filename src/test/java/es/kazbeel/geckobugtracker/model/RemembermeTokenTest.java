package es.kazbeel.geckobugtracker.model;


import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;

import es.kazbeel.geckobugtracker.builders.RemembermeTokenBuilder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class RemembermeTokenTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testUsernameCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        RemembermeToken rmt = RemembermeTokenBuilder.remembermeToken()
                                                    .withUsername(null)
                                                    .withSeries("series")
                                                    .withToken("token")
                                                    .build();
        
        session.save(rmt);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testUsernameCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        RemembermeToken rmt = RemembermeTokenBuilder.remembermeToken()
                                                    .withUsername(RandomStringUtils.randomAlphabetic(256))
                                                    .withSeries("series")
                                                    .withToken("token")
                                                    .build();
        
        session.save(rmt);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testSeriesCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        RemembermeToken rmt = RemembermeTokenBuilder.remembermeToken()
                                                    .withUsername("username")
                                                    .withSeries(null)
                                                    .withToken("token")
                                                    .build();
        
        session.save(rmt);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testSeriesCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        RemembermeToken rmt = RemembermeTokenBuilder.remembermeToken()
                                                    .withUsername("username")
                                                    .withSeries(RandomStringUtils.randomAlphabetic(256))
                                                    .withToken("token")
                                                    .build();
        
        session.save(rmt);
        session.flush();
        session.clear();
    }
    
    @Test(expected = org.hibernate.exception.ConstraintViolationException.class)
    @Transactional
    public void testUsernameAndSeriesAreUnique () {
    
        Session session = sessionFactory.getCurrentSession();
        
        RemembermeToken rmt1 = RemembermeTokenBuilder.remembermeToken()
                                                     .withUsername("username_1")
                                                     .withSeries("series_1")
                                                     .withToken("token")
                                                     .build();
        
        RemembermeToken rmt2 = RemembermeTokenBuilder.remembermeToken()
                                                     .withUsername("username_2")
                                                     .withSeries("series_2")
                                                     .withToken("token")
                                                     .build();
        
        session.save(rmt1);
        session.save(rmt2);
        session.flush();
        session.clear();
        
        // Username and series can be equal separately
        rmt2.setUsername(rmt1.getUsername());
        
        session.save(rmt2);
        session.flush();
        session.clear();
        
        // But not both
        rmt2.setSeries(rmt1.getSeries());
        session.save(rmt2);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testTokenCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        RemembermeToken rmt = RemembermeTokenBuilder.remembermeToken()
                                                    .withUsername("username")
                                                    .withSeries("series")
                                                    .withToken(null)
                                                    .build();
        
        session.save(rmt);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testTokenCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        RemembermeToken rmt = RemembermeTokenBuilder.remembermeToken()
                                                    .withUsername("username")
                                                    .withSeries("series")
                                                    .withToken(RandomStringUtils.randomAlphabetic(256))
                                                    .build();
        
        session.save(rmt);
        session.flush();
        session.clear();
    }
}
