package es.kazbeel.geckobugtracker.model;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class IssueTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("IssueTest.xml")
    @Transactional
    public void testIssueTypeCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 1);
        
        issue.setType(null);
        
        session.save(issue);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("IssueTest.xml")
    @Transactional
    public void testReporterCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 1);
        
        issue.setReporter(null);
        
        session.save(issue);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("IssueTest.xml")
    @Transactional
    public void testSummaryCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 1);
        
        issue.setSummary(null);
        
        session.save(issue);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("IssueTest.xml")
    @Transactional
    public void testSummaryCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 1);
        
        issue.setSummary(RandomStringUtils.randomAlphabetic(256));
        
        session.save(issue);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("IssueTest.xml")
    @Transactional
    public void testDescriptionCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 1);
        
        issue.setDescription(RandomStringUtils.randomAlphabetic(100001));
        
        session.save(issue);
        session.flush();
        session.clear();
    }
    
    @Test
    @DatabaseSetup("IssueTest.xml")
    @Transactional
    public void testIssueWithoutEnvironments () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 2);
        
        assertNotNull(issue);
        assertEquals(issue.getEnvironments().size(), 0);
    }
    
    @Test
    @DatabaseSetup("IssueTest.xml")
    @Transactional
    public void testIssueWithEnvironments () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 3);
        
        assertNotNull(issue);
        assertEquals(issue.getEnvironments().size(), 3);
    }
    
    @Test
    @DatabaseSetup("IssueTest.xml")
    @Transactional
    public void testIssueWithoutTags () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 3);
        
        assertNotNull(issue);
        assertEquals(issue.getTags().size(), 0);
    }
    
    @Test
    @DatabaseSetup("IssueTest.xml")
    @Transactional
    public void testIssueWithTags () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 2);
        
        assertNotNull(issue);
        assertEquals(issue.getTags().size(), 3);
    }
    
    @Test
    @DatabaseSetup("IssueTest.xml")
    @Transactional
    public void testIssueWithoutComments () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 2);
        
        assertNotNull(issue);
        assertEquals(issue.getComments().size(), 0);
    }
    
    @Test
    @DatabaseSetup("IssueTest.xml")
    @Transactional
    public void testIssueWithComments () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 1);
        
        assertNotNull(issue);
        assertEquals(issue.getComments().size(), 3);
    }
    
    @Test
    @DatabaseSetup("IssueTest.xml")
    @Transactional
    public void testCountNumberOfIssues () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Long totalIssues = (Long) session.createQuery("SELECT COUNT(*) FROM Issue").uniqueResult();
        
        assertEquals(totalIssues, new Long(3));
    }
    
}
