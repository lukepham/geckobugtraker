package es.kazbeel.geckobugtracker.model;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.GroupBuilder;
import es.kazbeel.geckobugtracker.builders.UserBuilder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class GroupTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testNameCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        Group group = GroupBuilder.group()
                                  .withName(null)
                                  .withDescription("description")
                                  .build();
        
        session.save(group);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testNameCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Group group = GroupBuilder.group()
                                  .withName(RandomStringUtils.randomAscii(256))
                                  .withDescription("description")
                                  .build();
        
        session.save(group);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testDescriptionCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Group group = GroupBuilder.group()
                                  .withName("name")
                                  .withDescription(RandomStringUtils.randomAscii(1001))
                                  .build();
        
        session.save(group);
        session.flush();
        session.clear();
    }
    
    @Test
    @DatabaseSetup("GroupTest.xml")
    @Transactional
    public void testGetUsers () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Group group = (Group) session.get(Group.class, 1);
        
        assertThat(group, is(not(nullValue())));
        assertThat(group.getUsers(), hasSize(3));
        assertThat(group.getUsers(),
                   containsInAnyOrder(UserBuilder.user().withId(1).withLoginName("login_name_1").build(),
                                      UserBuilder.user().withId(2).withLoginName("login_name_2").build(),
                                      UserBuilder.user().withId(3).withLoginName("login_name_3").build()));
    }
    
    @Test
    @DatabaseSetup("GroupTest.xml")
    @Transactional
    public void testGetUsersEmpty () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Group group = (Group) session.get(Group.class, 4);
        
        assertThat(group, is(not(nullValue())));
        assertThat(group.getUsers(), is(empty()));
    }
    
}
