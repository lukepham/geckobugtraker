package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.TagBuilder;
import es.kazbeel.geckobugtracker.model.Tag;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class TagDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private TagDao tagDao;
    
    
    @Test
    @DatabaseSetup("TagDaoTest.xml")
    @Transactional
    public void testFindAll () {
    
        List<Tag> tags = tagDao.findAll();
        
        assertThat(tags, is(not(nullValue())));
        assertThat(tags, hasSize(3));
        assertThat(tags, contains(TagBuilder.tag().withId(1).withName("tag_1").build(),
                                  TagBuilder.tag().withId(3).withName("tag_2").build(),
                                  TagBuilder.tag().withId(2).withName("tag_3").build()));
    }
    
    @Test
    @DatabaseSetup("TagDaoTest.xml")
    @Transactional
    public void testFindAllEnabled () {
    
        List<Tag> tags = tagDao.findAllEnabled();
        
        assertThat(tags, is(not(nullValue())));
        assertThat(tags, hasSize(2));
        assertThat(tags, contains(TagBuilder.tag().withId(1).withName("tag_1").build(),
                                  TagBuilder.tag().withId(3).withName("tag_2").build()));
    }
    
    @Test
    @DatabaseSetup("TagDaoTest.xml")
    @Transactional
    public void testFindById () {
    
        Tag tag = tagDao.findById(1);
        
        assertThat(tag, is(not(nullValue())));
        assertThat(tag, is(TagBuilder.tag().withId(1).withName("tag_1").build()));
    }
}
