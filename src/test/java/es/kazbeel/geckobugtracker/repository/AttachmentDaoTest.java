package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.AttachmentBuilder;
import es.kazbeel.geckobugtracker.model.Attachment;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class AttachmentDaoTest {
    
    @Autowired
    private AttachmentDao attachmentDao;
    
    
    @Test
    @DatabaseSetup("AttachmentDaoTest.xml")
    @Transactional
    public void testFindById () {
    
        Attachment attch = attachmentDao.findById(1);
        
        assertThat(attch, is(not(nullValue())));
        assertThat(attch, is(AttachmentBuilder.attachment().withId(1).build()));
    }
    
    @Test
    @DatabaseSetup("AttachmentDaoTest.xml")
    @Transactional
    public void findAllFromIssue () {
    
        List<Attachment> attchList = attachmentDao.findAllFromIssue(1);
        
        assertThat(attchList, is(not(nullValue())));
        assertThat(attchList, hasSize(2));
        assertThat(attchList, contains(AttachmentBuilder.attachment().withId(1).build(),
                                       AttachmentBuilder.attachment().withId(2).build()));
    }
}
