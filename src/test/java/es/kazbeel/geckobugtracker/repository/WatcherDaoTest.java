package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.WatcherBuilder;
import es.kazbeel.geckobugtracker.model.Watcher;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class WatcherDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private WatcherDao watcherDao;
    
    
    @Test
    @DatabaseSetup("WatcherDaoTest.xml")
    @Transactional
    public void testFindByUser () {
        List<Watcher> watchers = watcherDao.findByUser(1, 0, 10);
        
        assertThat(watchers, is(not(nullValue())));
        assertThat(watchers, hasSize(2));
        assertThat(watchers, contains(WatcherBuilder.watcher().withId(3).build(), WatcherBuilder.watcher().withId(5).build()));
    }

    @Test
    @DatabaseSetup("WatcherDaoTest.xml")
    @Transactional
    public void testFindByIssue () {
        List<Watcher> watchers = watcherDao.findByIssue(1);
        
        assertThat(watchers, is(not(nullValue())));
        assertThat(watchers, hasSize(2));
        assertThat(watchers, contains(WatcherBuilder.watcher().withId(2).build(), WatcherBuilder.watcher().withId(3).build()));
    }

    @Test
    @DatabaseSetup("WatcherDaoTest.xml")
    @Transactional
    public void testCountByUser () {
        Long count = watcherDao.countByUser(2);
        
        assertThat(count, is(not(nullValue())));
        assertThat(count, is(new Long(1)));
    }

    @Test
    @DatabaseSetup("WatcherDaoTest.xml")
    @Transactional
    public void testCountByIssue () {
        Long count = watcherDao.countByIssue(1);
        
        assertThat(count, is(not(nullValue())));
        assertThat(count, is(new Long(2)));
    }

    @Test
    @DatabaseSetup("WatcherDaoTest.xml")
    @Transactional
    public void testIsIssueWatchedByUser () {
    
        Boolean result = watcherDao.isIssueWatchedByUser(1, 2);
        
        assertThat(result, is(not(nullValue())));
        assertThat(result, is(true));
        
        result = watcherDao.isIssueWatchedByUser(2, 2);
        
        assertThat(result, is(not(nullValue())));
        assertThat(result, is(false));
    }
}
