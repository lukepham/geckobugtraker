package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.ResolutionBuilder;
import es.kazbeel.geckobugtracker.model.Resolution;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class ResolutionDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private ResolutionDao resolutionDao;
    
    
    @Test
    @DatabaseSetup("ResolutionDaoTest.xml")
    @Transactional
    public void testFindAll () {
    
        List<Resolution> resolutions = resolutionDao.findAll();
        
        assertThat(resolutions, is(not(nullValue())));
        assertThat(resolutions, hasSize(3));
        assertThat(resolutions, contains(ResolutionBuilder.resolution().withId(1).withName("resolution_1").build(),
                                         ResolutionBuilder.resolution().withId(3).withName("resolution_3").build(),
                                         ResolutionBuilder.resolution().withId(2).withName("resolution_2").build()));
    }
    
    @Test
    @DatabaseSetup("ResolutionDaoTest.xml")
    @Transactional
    public void testFindAllEnabled () {
    
        List<Resolution> resolutions = resolutionDao.findAllEnabled();
        
        assertThat(resolutions, is(not(nullValue())));
        assertThat(resolutions, hasSize(2));
        assertThat(resolutions, contains(ResolutionBuilder.resolution().withId(3).withName("resolution_3").build(),
                                         ResolutionBuilder.resolution().withId(2).withName("resolution_2").build()));
    }
    
    @Test
    @DatabaseSetup("ResolutionDaoTest.xml")
    @Transactional
    public void testFindById () {
    
        Resolution resolution = resolutionDao.findById(1);
        
        assertThat(resolution, is(not(nullValue())));
        assertThat(resolution, is(ResolutionBuilder.resolution().withId(1).withName("resolution_1").build()));
    }
    
    @Test
    @DatabaseSetup("ResolutionDaoTest.xml")
    @Transactional
    public void testIncreaseOrder () {
    
        resolutionDao.increaseOrder(3);
        Resolution resolution = resolutionDao.findById(3);
        assertThat(resolution.getSortValue(), is(1));
    }
    
    @Test
    @DatabaseSetup("ResolutionDaoTest.xml")
    @Transactional
    public void testDecreaseOrder () {
    
        resolutionDao.decreaseOrder(2);
        Resolution updatedResolution = resolutionDao.findById(2);
        assertThat(updatedResolution.getSortValue(), is(5));
    }
}
