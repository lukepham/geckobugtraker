package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.GroupBuilder;
import es.kazbeel.geckobugtracker.model.Group;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class GroupDaoTest {
    
    @Autowired
    private GroupDao groupDao;
    
    
    @Test
    @DatabaseSetup("GroupDaoTest.xml")
    @Transactional
    public void testFindAllPaged () {
    
        List<Group> groups = groupDao.findAll(0, 4);
        
        assertThat(groups, is(not(nullValue())));
        assertThat(groups, hasSize(4));
        assertThat(groups, containsInAnyOrder(GroupBuilder.group().withId(1).withName("group_name_1").build(),
                                              GroupBuilder.group().withId(2).withName("group_name_2").build(),
                                              GroupBuilder.group().withId(3).withName("group_name_3").build(),
                                              GroupBuilder.group().withId(4).withName("group_name_4").build()));
        
        groups = groupDao.findAll(4, 4);
        
        assertThat(groups, is(not(nullValue())));
        assertThat(groups, hasSize(3));
        assertThat(groups, containsInAnyOrder(GroupBuilder.group().withId(5).withName("group_name_5").build(),
                                              GroupBuilder.group().withId(6).withName("group_name_6").build(),
                                              GroupBuilder.group().withId(7).withName("group_name_7").build()));
    }
    
    @Test
    @SuppressWarnings("unchecked")
    @DatabaseSetup("GroupDaoTest.xml")
    @Transactional
    public void testFindAllEnabled () {
    
        List<Group> groups = groupDao.findAllEnabled();
        
        assertThat(groups, is(not(nullValue())));
        assertThat(groups, hasSize(4));
        assertThat(groups,
                   containsInAnyOrder(hasProperty("enabled", is(true)),
                                      hasProperty("enabled", is(true)),
                                      hasProperty("enabled", is(true)),
                                      hasProperty("enabled", is(true))));
    }
    
    @Test
    @DatabaseSetup("GroupDaoTest.xml")
    @Transactional
    public void testFindByIdFound () {
    
        Group group = groupDao.findById(5);
        
        assertThat(group, is(not(nullValue())));
        assertThat(group, is(GroupBuilder.group().withId(5).withName("group_name_5").build()));
    }
    
    @Test
    @DatabaseSetup("GroupDaoTest.xml")
    @Transactional
    public void testFindByIdNotFound () {
    
        Group group = groupDao.findById(8);
        
        assertThat(group, is(nullValue()));
    }
    
    @Test
    @DatabaseSetup("GroupDaoTest.xml")
    @Transactional
    public void testCountAll () {
    
        Long count = groupDao.countAll();
        
        assertThat(count, is(not(nullValue())));
        assertThat(count, is(new Long(7)));
    }
}
