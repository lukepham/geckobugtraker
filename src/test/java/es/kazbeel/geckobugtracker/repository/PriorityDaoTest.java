package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.PriorityBuilder;
import es.kazbeel.geckobugtracker.model.Priority;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class PriorityDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private PriorityDao priorityDao;
    
    
    @Test
    @DatabaseSetup("PriorityDaoTest.xml")
    @Transactional
    public void testFindAll () {
    
        List<Priority> priorities = priorityDao.findAll();
        
        assertThat(priorities, is(not(nullValue())));
        assertThat(priorities, hasSize(3));
        assertThat(priorities, contains(PriorityBuilder.priority().withId(1).withName("prio_1").build(),
                                        PriorityBuilder.priority().withId(3).withName("prio_3").build(),
                                        PriorityBuilder.priority().withId(2).withName("prio_2").build()));
    }
    
    @Test
    @DatabaseSetup("PriorityDaoTest.xml")
    @Transactional
    public void testFindAllEnabled () {
    
        List<Priority> priorities = priorityDao.findAllEnabled();
        
        assertThat(priorities, is(not(nullValue())));
        assertThat(priorities, hasSize(2));
        assertThat(priorities, contains(PriorityBuilder.priority().withId(3).withName("prio_3").build(),
                                        PriorityBuilder.priority().withId(2).withName("prio_2").build()));
    }
    
    @Test
    @DatabaseSetup("PriorityDaoTest.xml")
    @Transactional
    public void testFindById () {
    
        Priority priority = priorityDao.findById(1);
        
        assertThat(priority, is(not(nullValue())));
        assertThat(priority, is(PriorityBuilder.priority().withId(1).withName("prio_1").build()));
    }

    @Test
    @DatabaseSetup("PriorityDaoTest.xml")
    @Transactional
    public void testIncreaseOrder () {
    
        priorityDao.increaseOrder(3);
        Priority priority = priorityDao.findById(3);
        assertThat(priority.getSortValue(), is(0));
    }

    @Test
    @DatabaseSetup("PriorityDaoTest.xml")
    @Transactional
    public void testDecreaseOrder () {
        
        priorityDao.decreaseOrder(2);
        Priority updatedPriority = priorityDao.findById(2);
        assertThat(updatedPriority.getSortValue(), is(3));
    }
}
