<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Gecko - Edit Issue #${issue.id}</title>

    <link rel="stylesheet" href="<c:url value="/resources/uikit/css/uikit.almost-flat.css" />" type="text/css" />
    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script src="<c:url value="/resources/uikit/js/uikit.min.js" />"></script>
</head>

<body>

<p>@TODO Project - Component</p>

<h2>Edit Issue: ${issue.id}</h2>

<form:form class="uk-form uk-form-horizontal" modelAttribute="issuePackage" method="post">
    <div class="uk-form-row">
        <label class="uk-form-label" for="summary">Summary:</label>
        <div class="uk-form-comtrols uk-form-controls-text">
            <form:input path="issue.summary" type="text" class="uk-form-width-large" value="${issue.summary}" />
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="description">Description:</label>
        <div id="description" class="uk-form-comtrols uk-form-controls-text">
            <form:textarea path="issue.description" rows="10" class="uk-form-width-large" title="${issue.description}" />
        </div>
    </div>
    
    <div class="uk-form-row">
        <label class="uk-form-label" for="form-type">Type:</label>
        <div class="uk-form-comtrols uk-form-controls-text">
            <form:select path="issue.type">
                <form:options items="${types}" itemLabel="name" itemValue="id" />
            </form:select>
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="form-status">Status:</label>
        <div class="uk-form-comtrols uk-form-controls-text">
            <form:select path="issue.status">
                <form:options items="${statuses}" itemLabel="name" itemValue="id" />
            </form:select>
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="form-priority">Priority:</label>
        <div class="uk-form-comtrols uk-form-controls-text">
            <form:select path="issue.priority">
                <form:options items="${priorities}" itemLabel="name" itemValue="id" />
            </form:select>
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="form-resolution">Resolution:</label>
        <div class="uk-form-comtrols uk-form-controls-text">
            <form:select path="issue.resolution">
                <form:options items="${resolutions}" itemLabel="name" itemValue="id" />
            </form:select>
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="form-environment">Environments:</label>
        <div class="uk-form-comtrols uk-form-controls-text">
            <form:select path="issue.environments" multiple="true">
                <form:options items="${environments}" itemLabel="name" itemValue="id" />
            </form:select>
        </div>
        <form:input type="text" path="environmentStr" />
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="form-tag">Tags:</label>
        <div class="uk-form-comtrols uk-form-controls-text">
            <form:select path="issue.tags" multiple="true">
                <form:options items="${tags}" itemLabel="name" itemValue="id" />
            </form:select>
        </div>
        <form:input type="text" path="tagStr" />
    </div>
    
    <div class="uk-form-row">
        <label class="uk-form-label" for="form-reporter">Reporter:</label>
        <div class="uk-form-comtrols uk-form-controls-text">
            <form:select path="issue.reporter">
                <form:options items="${users}" itemLabel="loginName" itemValue="id" />
            </form:select>
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="form-assignee">Assignee:</label>
        <div class="uk-form-comtrols uk-form-controls-text">
            <form:select path="issue.assignee">
                <form:options items="${users}" itemLabel="loginName" itemValue="id" />
            </form:select>
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="comment">Comment:</label>
        <div id="comment" class="uk-form-comtrols uk-form-controls-text">
            <textarea id="comment-body" name="body" rows="10"
                class="uk-form-width-large" placeholder="Comment..."></textarea>
        </div>
    </div>

    <button type="submit" class="uk-button">Update</button>
    <a class="uk-button" href="<c:url value="/issues/${issue.id}" />">Cancel</a>
</form:form>

</body>
</html>