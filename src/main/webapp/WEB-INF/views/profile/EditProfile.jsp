<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>User Profile</title>
    
    <link rel="stylesheet" href="<c:url value="/resources/uikit/css/uikit.almost-flat.css" />" type="text/css" />
    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script src="<c:url value="/resources/uikit/js/uikit.min.js" />"></script>
    <link rel="stylesheet" href="<c:url value="/resources/css/docs.css" />" type="text/css" />
</head>

<body>

<jsp:include page="../TopNavBar.jsp" />


<!-- Add another sort of header with User's Avatar and Fullname -->
<div class="tm-middle">
	<div class="uk-container uk-container-center">
	
	    <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
	        <div class="tm-sidebar uk-width-medium-1-4">
	            <ul class="tm-nav uk-nav uk-nav-side" data-uk-nav>
                    <li class="uk-active"><a href="<c:url value="/profile/ViewProfile" />">Summary</a></li>
                    <li><a href="<c:url value="/profile/Security" />">Security</a></li>
	            </ul>
	        </div>
	        
	        <div class="tm-main uk-width-medium-3-4">
                <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
                    <div class="uk-width-1-1">
                    <div class="uk-panel">
                    <form:form class="uk-form uk-form-horizontal" modelAttribute="loggedUser" method="post">
		                <h2 class="uk-panel-title">Summary</h2>
		                                
		                <p>
		                  <a href="#">
                            <img class="uk-thumbnail" width="40" height="40" src="<c:url value="/resources/images/unknown_avatar.jpg"/>">
		                  </a>
		                </p>
                        
                        <div class="uk-form-row">
                            <label class="uk-form-label">
                                Username:
                                <form:input path="loginName"/>
                            </label>
                        </div>
		                
                        <div class="uk-form-row">
                            <label class="uk-form-label">
                                Real Name:
                                <form:input path="firstName"/>
                                <form:input path="lastName"/>
                            </label>
                        </div>

                        <div class="uk-form-row">
                            <label class="uk-form-label">
                                Email:
                                <form:input path="email"/>
                            </label>
                        </div>

                        <div class="uk-form-row">
                            <label class="uk-form-label">
                                Shortcut:
                                <form:input path="shortcut"/>
                            </label>
                        </div>
                        
                        <button type="submit" class="uk-button">Create</button>
                        <a class="uk-button" href="<c:url value="/profile/ViewProfile" />">Cancel</a>
		            </form:form>
		            </div>
                    </div>
                </div>

                <hr class="uk-grid-divider">
                
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <h1>Projects</h1>
                        <p></p>
                    </div>
                    
                    <div class="uk-width-1-2">
                        <h2>Leading</h2>
                        
                        <ul class="uk-list">
                            <li><a href="<c:url value="#" />" class="uk-link-reset">Project X</a></li>
                            <li><a href="<c:url value="#" />" class="uk-link-reset">Project Y</a></li>
                            <li><a href="<c:url value="#" />" class="uk-link-reset">Project Z</a></li>
                        </ul>
                    </div>
    
                    <div class="uk-width-1-2">
                        <h2>Collaborating</h2>
    
                        <ul class="uk-list">
                            <li><a href="<c:url value="#" />" class="uk-link-reset">Project A</a></li>
                            <li><a href="<c:url value="#" />" class="uk-link-reset">Project B</a></li>
                            <li><a href="<c:url value="#" />" class="uk-link-reset">Project C</a></li>
                        </ul>
                    </div>
                </div>
	        </div>
	        
	    </div>
	    
	</div>
</div>

</body>
</html>