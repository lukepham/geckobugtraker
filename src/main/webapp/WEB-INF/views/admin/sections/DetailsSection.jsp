<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Section Details</title>
    
    <link rel="stylesheet" href="<c:url value="/resources/uikit/css/uikit.almost-flat.css" />" type="text/css" />
    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script src="<c:url value="/resources/uikit/js/uikit.min.js" />"></script>
    <link rel="stylesheet" href="<c:url value="/resources/css/docs.css" />" type="text/css" />
</head>

<body>

<jsp:include page="../../TopNavBar.jsp" />


<div class="tm-middle">
	<div class="uk-container uk-container-center">
	
	    <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
            <div class="tm-sidebar uk-width-medium-1-4">
                <ul class="tm-nav uk-nav uk-nav-side" data-uk-nav>
                    <li><a href="<c:url value="/admin/users/ViewUsers" />">Users</a></li>
                    <li><a href="<c:url value="/admin/GroupsList" />">Groups</a></li>
                    <li><a href="<c:url value="/admin/IssueTypeList" />">Issue Types</a></li>
                    <li><a href="<c:url value="/admin/PriorityList" />">Priorities</a></li>
                    <li class="uk-active"><a href="<c:url value="/admin/resolutions/ViewResolutions" />">Resolutions</a></li>
                </ul>
            </div>
	        
	        <div class="tm-main uk-width-medium-3-4">
                <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
                    <div class="uk-width-1-1">
                    <div class="uk-panel">
                        <spring:url value="/admin/sections/EditSection" var="editSectionUrl" htmlEscape="true">
                            <spring:param name="id" value="${section.id}" />
                        </spring:url>
                        <spring:url value="/admin/sections/DeleteSection" var="delSectionUrl" htmlEscape="true">
                            <spring:param name="id" value="${section.id}" />
                        </spring:url>
                        <p>
                        <a class="uk-button" href="${editSectionUrl}">Edit</a>
                        <a class="uk-button" href="${delSectionUrl}">Delete</a>
                        </p>
                        
                        SUMARY <hr />
                        <p />
                        Name: <c:out value="${section.name}" /><br />
                        Description: <c:out value="${section.description}" /><br />
                        Position: <c:out value="${section.sortValue}" /><p />
                        
                        PROJECTS
                        <form action="<c:url value="/admin/projects/CreateProjectForm" />" method="post">
                            <input type="hidden" name="sectionId" value="${section.id}">
                            <button type="submit" class="uk-button uk-align-right"><i class="uk-icon-plus"></i> Create Project</button>
                        </form>
                        <hr />
                        <ul>
                        <c:forEach items="${section.projects}" var="project">
                            <spring:url value="/admin/projects/DetailsProject" var="projectUrl" htmlEscape="true">
                                <spring:param name="id" value="${project.id}" />
                            </spring:url>
                            <li><a href="${projectUrl}"><c:out value="${project.name}" /></a></li>
                        </c:forEach>
                        </ul>
		            </div>
                    </div>
                </div>
	        </div>
	        
	    </div>
	    
	</div>
</div>

</body>
</html>