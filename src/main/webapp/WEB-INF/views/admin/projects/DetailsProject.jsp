<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Project Details</title>
    
    <link rel="stylesheet" href="<c:url value="/resources/uikit/css/uikit.almost-flat.css" />" type="text/css" />
    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script src="<c:url value="/resources/uikit/js/uikit.min.js" />"></script>
    <link rel="stylesheet" href="<c:url value="/resources/css/docs.css" />" type="text/css" />
</head>

<body>

<jsp:include page="../../TopNavBar.jsp" />


<div class="tm-middle">
	<div class="uk-container uk-container-center">
	
	    <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
            <div class="tm-sidebar uk-width-medium-1-4">
                <ul class="tm-nav uk-nav uk-nav-side" data-uk-nav>
                    <li><a href="<c:url value="/admin/users/ViewUsers" />">Users</a></li>
                    <li><a href="<c:url value="/admin/GroupsList" />">Groups</a></li>
                    <li><a href="<c:url value="/admin/IssueTypeList" />">Issue Types</a></li>
                    <li><a href="<c:url value="/admin/PriorityList" />">Priorities</a></li>
                    <li class="uk-active"><a href="<c:url value="/admin/resolutions/ViewResolutions" />">Resolutions</a></li>
                </ul>
            </div>
	        
	        <div class="tm-main uk-width-medium-3-4">
                <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
                    <div class="uk-width-1-1">

                    <div class="uk-panel">
                        SUMARY
                        <p />
                        Name: <c:out value="${project.name}" /><p />
                        Description: <c:out value="${project.description}" /><p />
                        <spring:url value="/admin/sections/DetailsSection" var="sectionUrl" htmlEscape="true">
                            <spring:param name="id" value="${project.section.id}" />
                        </spring:url>
                        Section: <a href="${sectionUrl}"><c:out value="${project.section.name}" /></a><p />
                        
                        OWN COMPONENTS
                        <form action="<c:url value="/admin/components/CreateComponentForm" />" method="post">
                            <input type="hidden" name="projectId" value="${project.id}">
                            <button type="submit" class="uk-button uk-align-right"><i class="uk-icon-plus"></i> Create Component</button>
                        </form>
                        <p />
                        <c:forEach items="${project.children}" var="child">
                            <spring:url value="/admin/components/DetailsComponent" var="childUrl" htmlEscape="true">
                                <spring:param name="id" value="${child.id}" />
                            </spring:url>
                            <a href="${childUrl}"><c:out value="${child.name}" /></a><p />
                        </c:forEach>

                        SHARED COMPONENTS
                        <p />
                        <c:forEach items="${project.components}" var="component">
                            <spring:url value="/admin/components/DetailsComponent" var="componentUrl" htmlEscape="true">
                                <spring:param name="id" value="${component.id}" />
                            </spring:url>
                            <a href="${componentUrl}"><c:out value="${component.name}" /></a><p />
                        </c:forEach>

                        VERSIONS
                        <p />
                        <form action="<c:url value="/admin/projectversions/CreateProjectVersionForm" />" method="post">
                            <input type="hidden" name="projectId" value="${project.id}">
                            <button type="submit" class="uk-button uk-align-right"><i class="uk-icon-plus"></i> Create Version</button>
                        </form>
                        <c:forEach items="${project.versions}" var="version">
                            <spring:url value="/admin/projectversions/DetailsProjectVersion" var="projectVersionUrl" htmlEscape="true">
                                <spring:param name="id" value="${version.id}" />
                            </spring:url>
                            <a href="${projectVersionUrl}"><c:out value="${version.name}" /></a><p />
                        </c:forEach>
		            </div>
                    </div>
                </div>
	        </div>
	        
	    </div>
	    
	</div>
</div>

</body>
</html>