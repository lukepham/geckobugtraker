package es.kazbeel.geckobugtracker.support;


public class PageRequest {
    
    private int page;
    
    private int size;
    
    
    public PageRequest (int page, int size) {
    
        if (page < 0) {
            throw new IllegalArgumentException("Page number must be positive.");
        }
        
        if (size < 1) {
            throw new IllegalArgumentException("Page size must be greater or equal than 1.");
        }
        
        this.page = page;
        this.size = size;
    }
    
    public int getPage () {
    
        return page;
    }
    
    public int getSize () {
    
        return size;
    }
    
    public int getOffset () {
    
        return (page * size);
    }
}
