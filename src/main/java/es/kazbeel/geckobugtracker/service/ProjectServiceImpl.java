package es.kazbeel.geckobugtracker.service;


import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.kazbeel.geckobugtracker.model.Component;
import es.kazbeel.geckobugtracker.model.Project;
import es.kazbeel.geckobugtracker.model.ProjectVersion;
import es.kazbeel.geckobugtracker.model.Section;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.repository.ComponentDao;
import es.kazbeel.geckobugtracker.repository.ProjectDao;
import es.kazbeel.geckobugtracker.repository.ProjectVersionDao;
import es.kazbeel.geckobugtracker.repository.SectionDao;


@Service
public class ProjectServiceImpl implements ProjectService {
    
    private SectionDao sectionDao;
    
    private ProjectDao projectDao;
    
    private ProjectVersionDao projectVersionDao;
    
    private ComponentDao componentDao;
    
    
    @Autowired
    public ProjectServiceImpl (SectionDao sectionDao,
                               ProjectDao projectDao,
                               ProjectVersionDao projectVersionDao,
                               ComponentDao componentDao) {
    
        this.sectionDao = sectionDao;
        this.projectDao = projectDao;
        this.projectVersionDao = projectVersionDao;
        this.componentDao = componentDao;
    }
    
    @Override
    @Transactional
    public Collection<Section> getAllSections () {//TESTME
    
        Collection<Section> sections = sectionDao.findAll();
        for(Section s : sections) {
            s.getProjects().size();
        }
        
        return sections;
    }
    
    @Override
    @Transactional
    public Section getSectionById (Long id) {
    
        return sectionDao.findById(id);
    }
    
    @Override
    @Transactional
    public Section getSectionByIdWithProjects (Long id) {
    
        Section section = sectionDao.findById(id);
        
        if (section != null) {
            section.getProjects().size();
        }
        
        return section;
    }
    
    @Override
    @Transactional
    public Section getSectionByName (String name) {
    
        return sectionDao.findByName(name);
    }
    
    @Override
    @Transactional
    public void setHigherOrderToSection (Section section) {
    
        sectionDao.decreaseSortValue(section.getId());
    }
    
    @Override
    @Transactional
    public void setLowerOrderToSection (Section section) {
    
        sectionDao.increaseSortValue(section.getId());
    }
    
    @Override
    @Transactional
    public void saveSection (Section section) {
    
        sectionDao.saveOrUpdate(section);
    }
    
    @Override
    @Transactional
    public void deleteSection (Section section) {
    
        sectionDao.delete(section);
    }
    
    @Override
    @Transactional
    public Project getProjectById (Long projectId) {
    
        return projectDao.findById(projectId);
    }
    
    @Override
    @Transactional
    public Project getProjectByIdWithSharedComponentsAndVersions (Long projectId) {
    
        Project project = projectDao.findById(projectId);
        
        project.getChildren().size();
        project.getComponents().size();
        project.getVersions().size();
        
        return project;
    }
    
    @Override
    @Transactional
    public Collection<Project> getAllProjects () {
    
        return projectDao.findAll();
    }
    
    @Override
    @Transactional
    public Project getProjectByName (String name) {
    
        return projectDao.findByName(name);
    }
    
    @Override
    @Transactional
    public Collection<Project> getProjectsBySection (Section section) {
    
        return projectDao.findBySection(section.getId());
    }
    
    @Override
    @Transactional
    public Collection<Project> getProjectsByDevLeader (User devLeader) {
    
        return projectDao.findByDevLeader(devLeader.getId());
    }
    
    @Override
    @Transactional
    public Collection<Project> getProjectsByQALeader (User qaLeader) {
    
        return projectDao.findByQALeader(qaLeader.getId());
    }
    
    @Override
    @Transactional
    public void saveProject (Project project) {
    
        projectDao.saveOrUpdate(project);
    }
    
    @Override
    @Transactional
    public void deleteProject (Project project) {
    
        projectDao.delete(project);
    }
    
    @Override
    @Transactional
    public void saveProjectWithChildComponent (Project project) {//TESTME
        
        projectDao.saveOrUpdate(project);
        
        // TODO Copy data from Project to Component as an utility
        Component component = new Component();
        component.setName(project.getName());
        component.setDescription(project.getDescription());
        component.setDevLeader(project.getDevLeader());
        component.setQaLeader(project.getQaLeader());
        component.setRootProject(project);
        
        componentDao.saveOrUpdate(component);
    }
    
    @Override
    @Transactional
    public ProjectVersion getProjectVersionById (Long projectVersionId) {
    
        return projectVersionDao.findById(projectVersionId);
    }
    
    @Override
    @Transactional
    public Collection<ProjectVersion> getProjectVersionsByProject (Project project) {
    
        return projectVersionDao.findByProject(project.getId());
    }
    
    @Override
    @Transactional
    public void setHigherOrderToProjectVersion (ProjectVersion projectVersion) {
    
        projectVersionDao.decreaseSortValue(projectVersion.getId());
    }
    
    @Override
    @Transactional
    public void setLowerOrderToProjectVersion (ProjectVersion projectVersion) {
    
        projectVersionDao.increaseSortValue(projectVersion.getId());
    }
    
    @Override
    @Transactional
    public void saveProjectVersion (ProjectVersion projectVersion) {
    
        projectVersionDao.saveOrUpdate(projectVersion);
    }
    
    @Override
    @Transactional
    public void deleteProjectVersion (ProjectVersion projectVersion) {
    
        projectVersionDao.delete(projectVersion);
    }
    
    @Override
    @Transactional
    public Component getComponentById (Long componentId) {
    
        return componentDao.findById(componentId);
    }
    
    @Override
    @Transactional
    public Component getComponentByName (String name) {
    
        return componentDao.findByName(name);
    }
    
    @Override
    @Transactional
    public Collection<Component> getComponentsByProject (Project project) {
    
        return componentDao.findByProject(project.getId());
    }
    
    @Override
    @Transactional
    public void saveComponent (Component component) {
    
        componentDao.saveOrUpdate(component);
    }
    
    @Override
    @Transactional
    public void deleteComponent (Component component) {
    
        componentDao.delete(component);
    }
    
    @Override
    @Transactional
    public Component getComponentByIdWithSharedProjects (Long componentId) {
    
        Component component = componentDao.findById(componentId);
        
        if (component != null) {
            component.getProjects().size();
        }
        
        return component;
    }
    
    @Override
    @Transactional
    public Collection<Project> getShareableProjectsForComponent (Component component) {
    
        return projectDao.findShareableProjectsForComponent(component.getId());
    }
    
    @Override
    @Transactional
    public void shareComponentWithProject (Component component, Project project) {
    
        project.getComponents().add(component);
        
        projectDao.saveOrUpdate(project);
    }
    
    @Override
    @Transactional
    public void unshareCompronentFromProject (Component component, Project project) {
    
        project.getComponents().remove(component);
        
        projectDao.saveOrUpdate(project);
    }
}
