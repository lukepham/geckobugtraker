package es.kazbeel.geckobugtracker.service;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.kazbeel.geckobugtracker.model.Attachment;
import es.kazbeel.geckobugtracker.model.Category;
import es.kazbeel.geckobugtracker.model.Comment;
import es.kazbeel.geckobugtracker.model.Environment;
import es.kazbeel.geckobugtracker.model.Issue;
import es.kazbeel.geckobugtracker.model.IssueLink;
import es.kazbeel.geckobugtracker.model.IssueLinkType;
import es.kazbeel.geckobugtracker.model.IssueType;
import es.kazbeel.geckobugtracker.model.Priority;
import es.kazbeel.geckobugtracker.model.Resolution;
import es.kazbeel.geckobugtracker.model.Status;
import es.kazbeel.geckobugtracker.model.Tag;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.model.Vote;
import es.kazbeel.geckobugtracker.model.Watcher;
import es.kazbeel.geckobugtracker.repository.AttachmentDao;
import es.kazbeel.geckobugtracker.repository.CategoryDao;
import es.kazbeel.geckobugtracker.repository.CommentDao;
import es.kazbeel.geckobugtracker.repository.EnvironmentDao;
import es.kazbeel.geckobugtracker.repository.IssueDao;
import es.kazbeel.geckobugtracker.repository.IssueLinkDao;
import es.kazbeel.geckobugtracker.repository.IssueLinkTypeDao;
import es.kazbeel.geckobugtracker.repository.IssueTypeDao;
import es.kazbeel.geckobugtracker.repository.PriorityDao;
import es.kazbeel.geckobugtracker.repository.ResolutionDao;
import es.kazbeel.geckobugtracker.repository.StatusDao;
import es.kazbeel.geckobugtracker.repository.TagDao;
import es.kazbeel.geckobugtracker.repository.VoteDao;
import es.kazbeel.geckobugtracker.repository.WatcherDao;
import es.kazbeel.geckobugtracker.support.Page;
import es.kazbeel.geckobugtracker.support.PageRequest;


@Service
public class IssueServiceImpl implements IssueService {
    
    private IssueDao issueDao;
    
    private IssueTypeDao issueTypeDao;
    
    private StatusDao statusDao;
    
    private PriorityDao priorityDao;
    
    private EnvironmentDao environmentDao;
    
    private TagDao tagDao;
    
    private IssueLinkDao issueLinkDao;
    
    private VoteDao voteDao;
    
    private WatcherDao watcherDao;
    
    private CommentDao commentDao;
    
    private ResolutionDao resolutionDao;
    
    private CategoryDao categoryDao;
    
    private IssueLinkTypeDao issueLinkTypeDao;
    
    private AttachmentDao attachmentDao;
    
    
    @Autowired
    public IssueServiceImpl (IssueDao issueDao,
                             IssueTypeDao issueTypeDao,
                             IssueLinkTypeDao issueLinkTypeDao,
                             StatusDao statusDao,
                             PriorityDao priorityDao,
                             EnvironmentDao environmentDao,
                             TagDao tagDao,
                             IssueLinkDao issueLinkDao,
                             VoteDao voteDao,
                             WatcherDao watcherDao,
                             CommentDao commentDao,
                             ResolutionDao resolutionDao,
                             CategoryDao categoryDao,
                             AttachmentDao attachmentDao) {
    
        this.issueDao = issueDao;
        this.issueTypeDao = issueTypeDao;
        this.issueLinkTypeDao = issueLinkTypeDao;
        this.statusDao = statusDao;
        this.priorityDao = priorityDao;
        this.environmentDao = environmentDao;
        this.tagDao = tagDao;
        this.issueLinkDao = issueLinkDao;
        this.voteDao = voteDao;
        this.watcherDao = watcherDao;
        this.commentDao = commentDao;
        this.resolutionDao = resolutionDao;
        this.categoryDao = categoryDao;
        this.attachmentDao = attachmentDao;
    }
    
    /* Issues */
    
    @Override
    @Transactional
    public Issue getIssueById (Integer issueId) throws DataAccessException {
    
        return issueDao.findById(issueId);
    }
    
    @Override
    @Transactional
    public Page<Issue> getIssuesByReporter (User reporter, PageRequest pageRequest) throws DataAccessException {
    
        List<Issue> content = issueDao.findByReporter(reporter.getId(), pageRequest.getOffset(), pageRequest.getSize());
        int totalElements = issueDao.countByReporter(reporter.getId()).intValue();
        
        Page<Issue> page = new Page<Issue>(content);
        page.setPageNumber(pageRequest.getPage());
        page.setMaxPageSize(pageRequest.getSize());
        page.setTotalElements(totalElements);
        
        return page;
    }
    
    @Override
    @Transactional
    public Page<Issue> getIssuesVotedByUser (User voter, PageRequest pageRequest) throws DataAccessException {
    
        List<Issue> content = new ArrayList<Issue>();
        
        for (Vote vote : voteDao.findByUser(voter.getId(), pageRequest.getOffset(), pageRequest.getSize())) {
            content.add(vote.getIssue());
        }
        
        int totalElements = voteDao.countByUser(voter.getId()).intValue();
        
        Page<Issue> page = new Page<Issue>(content);
        page.setPageNumber(pageRequest.getPage());
        page.setMaxPageSize(pageRequest.getSize());
        page.setTotalElements(totalElements);
        
        return page;
    }
    
    @Override
    @Transactional
    public Page<Issue> getIssuesWatchedByUser (User watcher, PageRequest pageRequest) throws DataAccessException {
    
        List<Issue> content = new ArrayList<Issue>();
        
        for (Watcher _watcher : watcherDao.findByUser(watcher.getId(), pageRequest.getOffset(), pageRequest.getSize())) {
            content.add(_watcher.getIssue());
        }
        
        int totalElements = watcherDao.countByUser(watcher.getId()).intValue();
        
        Page<Issue> page = new Page<Issue>(content);
        page.setPageNumber(pageRequest.getPage());
        page.setMaxPageSize(pageRequest.getSize());
        page.setTotalElements(totalElements);
        
        return page;
    }
    
    @Override
    @Transactional
    public Page<Issue> getAllIssues (PageRequest pageRequest) throws DataAccessException {
    
        List<Issue> content = issueDao.findAll(pageRequest.getOffset(), pageRequest.getSize());
        int totalElements = issueDao.countAll().intValue();
        
        Page<Issue> page = new Page<Issue>(content);
        page.setPageNumber(pageRequest.getPage());
        page.setMaxPageSize(pageRequest.getSize());
        page.setTotalElements(totalElements);
        
        return page;
    }
    
    @Override
    @Transactional
    public Page<Issue> getIssuesByAssigneeAndResolution (User assignee, Resolution resolution, PageRequest pageRequest) throws DataAccessException {
    
        List<Issue> content = issueDao.findByAssigneeAndResolution(assignee.getId(),
                                                                   resolution.getId(),
                                                                   pageRequest.getOffset(),
                                                                   pageRequest.getSize());
        int totalElements = issueDao.countByAssigneeAndResolution(assignee.getId(), resolution.getId()).intValue();
        
        Page<Issue> page = new Page<Issue>(content);
        page.setPageNumber(pageRequest.getPage());
        page.setMaxPageSize(pageRequest.getSize());
        page.setTotalElements(totalElements);
        
        return page;
    }
    
    @Override
    @Transactional
    public Collection<Issue> getAddedRecentlyIssuesTable () throws DataAccessException {
    
        // TODO
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -7);
        
        return issueDao.findAddedRecentlyTable(cal.getTime());
    }
    
    @Override
    @Transactional
    public void saveIssue (Issue issue) throws DataAccessException {
    
        issueDao.saveOrUpdate(issue);
    }
    
    @Override
    @Transactional
    public Long getNumIssuesByReporter (User reporter) throws DataAccessException {
    
        return issueDao.countByReporter(reporter.getId());
    }
    
    @Override
    @Transactional
    public Long getNumIssuesWatchedByUser (User watcher) throws DataAccessException {
    
        return watcherDao.countByUser(watcher.getId());
    }
    
    @Override
    @Transactional
    public Long getNumIssuesVotedByUser (User voter) throws DataAccessException {
    
        return voteDao.countByUser(voter.getId());
    }
    
    @Override
    @Transactional
    public void deleteIssue (Issue issue) throws DataAccessException {
    
        issue.getComments().clear();
        
        List<Vote> votes = voteDao.findByIssue(issue.getId());
        
        for (Vote vote : votes) {
            voteDao.delete(vote);
        }
        
        List<Watcher> watchers = watcherDao.findByIssue(issue.getId());
        
        for (Watcher watcher : watchers) {
            watcherDao.delete(watcher);
        }
        
        List<IssueLink> issueLinks = issueLinkDao.findAllRelatedToIssue(issue.getId());
        
        for (IssueLink issueLink : issueLinks) {
            issueLinkDao.delete(issueLink);
        }
        
        issueDao.delete(issue);
    }
    
    /* Comments */
    
    @Override
    @Transactional
    public void addCommentToIssue (Issue issue, User user, Comment comment) throws DataAccessException {
    
        comment.setAuthor(user);
        comment.setIssue(issue);
        
        commentDao.save(comment);
    }
    
    /* Votes */
    
    @Override
    @Transactional
    public Long getNumVotesForIssue (Issue issue) throws DataAccessException {
    
        return voteDao.countByIssue(issue.getId());
    }
    
    @Override
    @Transactional
    public Boolean isIssueVotedByUser (Issue issue, User voter) throws DataAccessException {
    
        return voteDao.isIssueVotedByUser(issue.getId(), voter.getId());
    }
    
    @Override
    @Transactional
    public void voteIssue (User user, Issue issue, boolean vote) throws DataAccessException {
    
        Collection<Vote> votes = voteDao.findByIssue(issue.getId());
        
        for (Vote v : votes) {
            if ((v.getUser().equals(user) == true) &&
                (vote == false)) {
                voteDao.delete(v);
                return;
            }
        }
        
        Vote v = new Vote(user, issue);
        voteDao.save(v);
    }
    
    /* Watchers */
    
    @Override
    @Transactional
    public Long getNumWatchersForIssue (Issue issue) throws DataAccessException {
    
        return watcherDao.countByIssue(issue.getId());
    }
    
    @Override
    @Transactional
    public Boolean isIssueWatchedByUser (Issue issue, User watcher) throws DataAccessException {
    
        return watcherDao.isIssueWatchedByUser(issue.getId(), watcher.getId());
    }
    
    @Override
    @Transactional
    public void watchIssue (User user, Issue issue, boolean watch) throws DataAccessException {
    
        Collection<Watcher> watchers = watcherDao.findByIssue(issue.getId());
        
        for (Watcher w : watchers) {
            if ((w.getUser().equals(user) == true) && (watch == false)) {
                watcherDao.delete(w);
                return;
            }
        }
        
        Watcher w = new Watcher(user, issue);
        watcherDao.save(w);
    }
    
    /* Category */
    
    @Override
    @Transactional
    public void saveCategory (Category category) throws DataAccessException {
    
        categoryDao.saveOrUpdate(category);
    }
    
    @Override
    @Transactional
    public void deleteCategory (Category category) throws DataAccessException {
    
        categoryDao.delete(category);
    }
    
    @Override
    @Transactional
    public Collection<Category> getAllCategories () throws DataAccessException {
    
        return categoryDao.findAll();
    }
    
    @Override
    @Transactional
    public Category getCategoryById (Integer id) throws DataAccessException {
    
        return categoryDao.findById(id);
    }
    
    /* Issue Types */
    
    @Override
    @Transactional
    public void saveIssueType (IssueType issueType) throws DataAccessException {
    
        issueTypeDao.saveOrUpdate(issueType);
    }
    
    @Override
    @Transactional
    public void deleteIssueType (IssueType issueType) throws DataAccessException {
    
        issueTypeDao.delete(issueType);
    }
    
    @Override
    @Transactional
    public Collection<IssueType> getAllIssueTypes () throws DataAccessException {
    
        return issueTypeDao.findAll();
    }
    
    @Override
    @Transactional
    public IssueType getIssueTypeById (Integer id) throws DataAccessException {
    
        return issueTypeDao.findById(id);
    }
    
    /* Issue Link Types */
    
    @Override
    @Transactional
    public void saveIssueLinkType (IssueLinkType issueLinkType) throws DataAccessException {
    
        issueLinkTypeDao.saveOrUpdate(issueLinkType);
    }
    
    @Override
    @Transactional
    public void deleteIssueLinkType (IssueLinkType issueLinkType) throws DataAccessException {
    
        issueLinkTypeDao.delete(issueLinkType);
    }
    
    @Override
    @Transactional
    public Collection<IssueLinkType> getAllIssueLinkTypes () throws DataAccessException {
    
        return issueLinkTypeDao.findAll();
    }
    
    @Override
    @Transactional
    public IssueLinkType getIssueLinkTypeById (Integer id) throws DataAccessException {
    
        return issueLinkTypeDao.findById(id);
    }
    
    /* Issue Links */
    
    @Override
    @Transactional
    public IssueLink getIssueLinkById (Integer id) {

        return issueLinkDao.findById(id);
    }
    
    @Override
    @Transactional
    public Collection<IssueLink> getAllIssueLinksWithSource (Issue source) throws DataAccessException {
    
        return issueLinkDao.findAllWithSource(source.getId());
    }
    
    @Override
    @Transactional
    public Collection<IssueLink> getAllIssueLinksWithDestination (Issue destination) throws DataAccessException {
    
        return issueLinkDao.findAllWithDestination(destination.getId());
    }
    
    @Override
    @Transactional
    public Collection<IssueLink> getAllIssueLinksRelatedToIssue (Issue issue) throws DataAccessException {
    
        return issueLinkDao.findAllRelatedToIssue(issue.getId());
    }
    
    @Override
    @Transactional
    public void saveIssueLink (IssueLink issueLink) throws DataAccessException {
    
        issueLinkDao.saveOrUpdate(issueLink);
    }
    
    @Override
    @Transactional
    public void deleteIssueLink (IssueLink issueLink) throws DataAccessException {
    
        issueLinkDao.delete(issueLink);
    }
    
    /* Statuses */
    
    @Override
    @Transactional
    public void saveStatus (Status status) throws DataAccessException {
    
        statusDao.saveOrUpdate(status);
    }
    
    @Override
    @Transactional
    public void deleteStatus (Status status) throws DataAccessException {
    
        statusDao.delete(status);
    }
    
    @Override
    @Transactional
    public Collection<Status> getAllStatuses () throws DataAccessException {
    
        return statusDao.findAll();
    }
    
    @Override
    @Transactional
    public Status getStatusById (Integer id) throws DataAccessException {
    
        return statusDao.findById(id);
    }
    
    /* Priorities */
    
    @Override
    @Transactional
    public void savePriority (Priority priority) throws DataAccessException {
    
        priorityDao.saveOrUpdate(priority);
    }
    
    @Override
    @Transactional
    public void deletePriority (Priority priority) throws DataAccessException {
    
        priorityDao.delete(priority);
    }
    
    @Override
    @Transactional
    public Collection<Priority> getAllPriorities () throws DataAccessException {
    
        return priorityDao.findAll();
    }
    
    @Override
    @Transactional
    public Priority getPriorityById (Integer id) throws DataAccessException {
    
        return priorityDao.findById(id);
    }
    
    @Override
    @Transactional
    public void setHigherOrderForPriority (Priority priority) throws DataAccessException {
    
        priorityDao.increaseOrder(priority.getId());
    }
    
    @Override
    @Transactional
    public void setLowerOrderForPriority (Priority priority) throws DataAccessException {
    
        priorityDao.decreaseOrder(priority.getId());
    }
    
    /* Resolutions */
    
    @Override
    @Transactional
    public void saveResolution (Resolution resolution) throws DataAccessException {
    
        resolutionDao.saveOrUpdate(resolution);
    }
    
    @Override
    @Transactional
    public void deleteResolution (Resolution resolution) throws DataAccessException {
    
        resolutionDao.delete(resolution);
    }
    
    @Override
    @Transactional
    public Collection<Resolution> getAllResolutions () throws DataAccessException {
    
        return resolutionDao.findAll();
    }
    
    @Override
    @Transactional
    public Resolution getResolutionById (Integer id) throws DataAccessException {
    
        return resolutionDao.findById(id);
    }
    
    @Override
    @Transactional
    public void setHigherOrderToResolution (Resolution resolution) throws DataAccessException {
    
        resolutionDao.increaseOrder(resolution.getId());
    }
    
    @Override
    @Transactional
    public void setLowerOrderToResolution (Resolution resolution) throws DataAccessException {
    
        resolutionDao.decreaseOrder(resolution.getId());
    }
    
    /* Environments */
    
    @Override
    @Transactional
    public Collection<Environment> getAllEnvironments () throws DataAccessException {
    
        return environmentDao.findAll();
    }
    
    @Override
    @Transactional
    public Environment getEnvironmentById (Integer id) throws DataAccessException {
    
        return environmentDao.findById(id);
    }
    
    @Override
    @Transactional
    public void saveEnvironment (Environment environment) {
    
        environmentDao.saveOrUpdate(environment);
    }
    
    @Override
    @Transactional
    public void deleteEnvironment (Environment environment) throws DataAccessException {
    
        environmentDao.delete(environment);
    }
    
    /* Tags */
    
    @Override
    @Transactional
    public Collection<Tag> getAllTags () throws DataAccessException {
    
        return tagDao.findAll();
    }
    
    @Override
    @Transactional
    public Tag getTagById (Integer id) throws DataAccessException {
    
        return tagDao.findById(id);
    }
    
    @Override
    @Transactional
    public void saveTag (Tag tag) throws DataAccessException {
    
        tagDao.saveOrUpdate(tag);
    }
    
    @Override
    @Transactional
    public void deleteTag (Tag tag) throws DataAccessException {
    
        tagDao.delete(tag);
    }
    
    /* Attachment */
    
    @Override
    @Transactional
    public void saveAttachment (Attachment attachment) throws DataAccessException {
    
        attachmentDao.saveOrUpdate(attachment);
    }
    
    @Override
    @Transactional
    public Attachment getAttachmentById (Integer attachmentId) throws DataAccessException {
    
        return attachmentDao.findById(attachmentId);
    }
    
    @Override
    @Transactional
    public Collection<Attachment> getAllAttachmentsFromIssue (Issue issue) throws DataAccessException {
    
        return attachmentDao.findAllFromIssue(issue.getId());
    }
}
