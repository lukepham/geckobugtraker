package es.kazbeel.geckobugtracker.model;


import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "COMPONENTS")
@NamedQueries({@NamedQuery(name = "Component.findByName", query = "FROM Component WHERE name = :name"),
               @NamedQuery(name = "Component.findByProjectId", query = "FROM Component WHERE rootProject = :pid ORDER BY id ASC")})
public class Component implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;
    
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "NAME", length = 255, unique = true, nullable = false)
    private String name;
    
    @Size(max = 4096)
    @Column(name = "DESCRIPTION", length = 4096)
    private String description;
    
    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "DEV_LEADER_ID", nullable = false)
    private User devLeader;
    
    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "QA_LEADER_ID", nullable = false)
    private User qaLeader;
    
    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "PROJECT_ID")
    private Project rootProject;
    
    @ManyToMany(mappedBy = "components")
    private Set<Project> projects = new HashSet<Project>(0);
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_ON", nullable = false, updatable = false)
    private Date createdOn;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_UPDATE_ON", nullable = false)
    private Date lastUpdateOn;
    
    
    public Component () {
    
    }
    
    public Long getId () {
    
        return id;
    }
    
    public void setId (Long id) {
    
        this.id = id;
    }
    
    public String getName () {
    
        return name;
    }
    
    public void setName (String name) {
    
        this.name = name;
    }
    
    public String getDescription () {
    
        return description;
    }
    
    public void setDescription (String description) {
    
        this.description = description;
    }
    
    public User getDevLeader () {
    
        return devLeader;
    }
    
    public void setDevLeader (User devLeader) {
    
        this.devLeader = devLeader;
    }
    
    public User getQaLeader () {
    
        return qaLeader;
    }
    
    public void setQaLeader (User qaLeader) {
    
        this.qaLeader = qaLeader;
    }
    
    public Project getRootProject () {
    
        return rootProject;
    }
    
    public void setRootProject (Project rootProject) {
    
        this.rootProject = rootProject;
    }
    
    public Set<Project> getProjects () {
    
        return projects;
    }
    
    public void setProjects (Set<Project> projects) {
    
        this.projects = projects;
    }
    
    public Date getCreatedOn () {
    
        return createdOn;
    }
    
    public void setCreatedOn (Date createdOn) {
    
        this.createdOn = createdOn;
    }
    
    public Date getLastUpdateOn () {
    
        return lastUpdateOn;
    }
    
    public void setLastUpdateOn (Date lastUpdateOn) {
    
        this.lastUpdateOn = lastUpdateOn;
    }
    
    @Override
    public int hashCode () {
    
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
    
    @Override
    public boolean equals (Object obj) {
    
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Component))
            return false;
        Component other = (Component) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
}
