package es.kazbeel.geckobugtracker.model;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "PROJECTS")
@NamedQueries({@NamedQuery(name = "Project.findAll", query = "FROM Project ORDER BY section ASC, id ASC"),
               @NamedQuery(name = "Project.findBySectionId", query = "FROM Project WHERE section = :id ORDER BY id ASC"),
               @NamedQuery(name = "Project.findByName", query = "FROM Project WHERE name = :name"),
               @NamedQuery(name = "Project.findByDevLeader", query = "FROM Project WHERE devLeader = :id"),
               @NamedQuery(name = "Project.findByQALeader", query = "FROM Project WHERE qaLeader = :id"),
               @NamedQuery(name = "Project.findShareableProjects",
                           query = "FROM Project AS p WHERE (p.id NOT IN (SELECT rootProject.id FROM Component c WHERE c.id = :cid)) AND (p.id NOT IN (SELECT pp.id FROM Project pp JOIN pp.components cc WHERE cc.id = :cid))")})
public class Project implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;
    
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "NAME", length = 255, unique = true, nullable = false)
    private String name;
    
    @Size(max = 4096)
    @Column(name = "DESCRIPTION", length = 4096)
    private String description;
    
    @NotNull
    @ManyToOne
    @JoinColumn(name = "SECTION_ID", nullable = false)
    private Section section;
    
    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "DEV_LEADER_ID", nullable = false)
    private User devLeader;
    
    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "QA_LEADER_ID", nullable = false)
    private User qaLeader;
    
    @OneToMany(mappedBy = "rootProject", cascade = CascadeType.REMOVE)
    private Set<Component> children;
    
    @ManyToMany
    @JoinTable(name = "PROJECTS_COMPONENTS_MAP",
               joinColumns = {@JoinColumn(name = "PROJECT_ID", nullable = false, updatable = false)},
               inverseJoinColumns = {@JoinColumn(name = "COMPONENT_ID", nullable = false, updatable = false)})
    private Set<Component> components = new HashSet<Component>(0);
    
    @OneToMany(mappedBy = "project", cascade = CascadeType.REMOVE)
    private Set<ProjectVersion> versions = new HashSet<ProjectVersion>(0);
    
    
    public Project () {
    
    }
    
    public Long getId () {
    
        return id;
    }
    
    public void setId (Long id) {
    
        this.id = id;
    }
    
    public String getName () {
    
        return name;
    }
    
    public void setName (String name) {
    
        this.name = name;
    }
    
    public String getDescription () {
    
        return description;
    }
    
    public void setDescription (String description) {
    
        this.description = description;
    }
    
    public Section getSection () {
    
        return section;
    }
    
    public void setSection (Section section) {
    
        this.section = section;
    }
    
    public User getDevLeader () {
    
        return devLeader;
    }
    
    public void setDevLeader (User devLeader) {
    
        this.devLeader = devLeader;
    }
    
    public User getQaLeader () {
    
        return qaLeader;
    }
    
    public void setQaLeader (User qaLeader) {
    
        this.qaLeader = qaLeader;
    }
    
    public Set<Component> getChildren () {
    
        return children;
    }
    
    public void setChildren (Set<Component> children) {
    
        this.children = children;
    }
    
    public Set<Component> getComponents () {
    
        return components;
    }
    
    public void setComponents (Set<Component> components) {
    
        this.components = components;
    }
    
    public Set<ProjectVersion> getVersions () {
    
        return versions;
    }
    
    public void setVersions (Set<ProjectVersion> versions) {
    
        this.versions = versions;
    }
    
    @Override
    public int hashCode () {
    
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
    
    @Override
    public boolean equals (Object obj) {
    
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Project))
            return false;
        Project other = (Project) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
}
