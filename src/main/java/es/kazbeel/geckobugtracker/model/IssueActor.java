package es.kazbeel.geckobugtracker.model;


import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;


public class IssueActor implements Serializable {
    
    private static final long serialVersionUID = -1;
    
    private Integer id;
    
    @NotNull
    private User user;
    
    @NotNull
    private Issue issue;
    
    private Date createdOn;
    
    
    protected IssueActor () {
    
    }
    
    protected IssueActor (User user, Issue issue) {
    
        this.user = user;
        this.issue = issue;
    }
    
    public Integer getId () {
    
        return id;
    }
    
    public void setId (Integer id) {
    
        this.id = id;
    }
    
    public User getUser () {
    
        return user;
    }
    
    public void setUser (User user) {
    
        this.user = user;
    }
    
    public Issue getIssue () {
    
        return issue;
    }
    
    public void setIssue (Issue issue) {
    
        this.issue = issue;
    }
    
    public Date getCreatedOn () {
    
        return createdOn;
    }
    
    public void setCreatedOn (Date createdOn) {
    
        this.createdOn = createdOn;
    }
    
    @Override
    public int hashCode () {
    
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
    
    @Override
    public boolean equals (Object obj) {
    
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        IssueActor other = (IssueActor) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }
}
