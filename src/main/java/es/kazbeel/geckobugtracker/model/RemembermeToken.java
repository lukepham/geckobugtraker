package es.kazbeel.geckobugtracker.model;


import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class RemembermeToken implements Serializable {
    
    private static final long serialVersionUID = -1;
    
    private Integer id;
    
    @NotNull
    @Size(min = 1, max = 255)
    private String username;
    
    @NotNull
    @Size(min = 1, max = 255)
    private String series;
    
    @NotNull
    @Size(min = 1, max = 255)
    private String token;
    
    private Date lastUpdateOn;
    
    
    public RemembermeToken () {
    
    }
    
    public Integer getId () {
    
        return id;
    }
    
    public void setId (Integer id) {
    
        this.id = id;
    }
    
    public String getUsername () {
    
        return username;
    }
    
    public void setUsername (String username) {
    
        this.username = username;
    }
    
    public String getSeries () {
    
        return series;
    }
    
    public void setSeries (String series) {
    
        this.series = series;
    }
    
    public String getToken () {
    
        return token;
    }
    
    public void setToken (String token) {
    
        this.token = token;
    }
    
    public Date getLastUpdateOn () {
    
        return lastUpdateOn;
    }
    
    public void setLastUpdateOn (Date lastUpdateOn) {
    
        this.lastUpdateOn = lastUpdateOn;
    }
    
    @Override
    public int hashCode () {
    
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((series == null) ? 0 : series.hashCode());
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }
    
    @Override
    public boolean equals (Object obj) {
    
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RemembermeToken other = (RemembermeToken) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (series == null) {
            if (other.series != null)
                return false;
        } else if (!series.equals(other.series))
            return false;
        if (username == null) {
            if (other.username != null)
                return false;
        } else if (!username.equals(other.username))
            return false;
        return true;
    }
}
