package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.Resolution;
import es.kazbeel.geckobugtracker.repository.ResolutionDao;


@Repository
public class ResolutionDaoImpl extends BaseDaoImpl<Resolution> implements ResolutionDao {
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Resolution> findAll () {
    
        return getSession().getNamedQuery("Resolution_findAll").list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Resolution> findAllEnabled () {
    
        return getSession().getNamedQuery("Resolution_findAllEnabled").list();
    }
    
    @Override
    public Resolution findById (Integer id) {
    
        return (Resolution) getSession().getNamedQuery("Resolution_findById").setInteger("id", id).uniqueResult();
    }

    @Override
    public void increaseOrder (Integer id) {
    
        getSession().getNamedQuery("Resolution_decreaseSortValue").setInteger("resolutionId", id).executeUpdate();
    }

    @Override
    public void decreaseOrder (Integer id) {
    
        getSession().getNamedQuery("Resolution_increaseSortValue").setInteger("resolutionId", id).executeUpdate();
    }
}
