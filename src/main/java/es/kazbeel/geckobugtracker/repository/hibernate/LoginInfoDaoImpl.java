package es.kazbeel.geckobugtracker.repository.hibernate;


import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.LoginInfo;
import es.kazbeel.geckobugtracker.repository.LoginInfoDao;


@Repository
public class LoginInfoDaoImpl extends BaseDaoImpl<LoginInfo> implements LoginInfoDao {
    
    @Override
    public LoginInfo findByUsername (String username) {
    
        return (LoginInfo) getSession().getNamedQuery("LoginInfo_findByUsername")
                                       .setString("username", username)
                                       .uniqueResult();
    }
    
    @Override
    public void resetFailAttempts (String username) {
    
        getSession().getNamedQuery("LoginInfo_resetFailAttempts")
                    .setString("username", username)
                    .executeUpdate();
    }
}
