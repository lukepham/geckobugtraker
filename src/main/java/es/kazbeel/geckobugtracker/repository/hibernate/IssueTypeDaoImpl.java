package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.IssueType;
import es.kazbeel.geckobugtracker.repository.IssueTypeDao;


@Repository
public class IssueTypeDaoImpl extends BaseDaoImpl<IssueType> implements IssueTypeDao {
    
    @Override
    @SuppressWarnings("unchecked")
    public List<IssueType> findAll () {
    
        return getSession().getNamedQuery("IssueType_findAll").list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<IssueType> findAllEnabled () {
    
        return getSession().getNamedQuery("IssueType_findAllEnabled").list();
    }
    
    @Override
    public IssueType findById (Integer id) {
    
        return (IssueType) getSession().get(IssueType.class, id);
    }
}
