package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.Watcher;
import es.kazbeel.geckobugtracker.repository.WatcherDao;


@Repository
public class WatcherDaoImpl extends BaseDaoImpl<Watcher> implements WatcherDao {
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Watcher> findByUser (Integer userId, int offset, int size) {
    
        return getSession().getNamedQuery("Watcher_findByUser")
                           .setFirstResult(offset)
                           .setMaxResults(size)
                           .setInteger("userId", userId)
                           .list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Watcher> findByIssue (Integer issueId) {
    
        return getSession().getNamedQuery("Watcher_findByIssue").setInteger("issueId", issueId).list();
    }
    
    @Override
    public Long countByUser (Integer userId) {
    
        return (Long) getSession().getNamedQuery("Watcher_countByUser").setInteger("userId", userId).uniqueResult();
    }
    
    @Override
    public Long countByIssue (Integer issueId) {
    
        return (Long) getSession().getNamedQuery("Watcher_countByIssue").setInteger("issueId", issueId).uniqueResult();
    }
    
    public Boolean isIssueWatchedByUser (Integer issueId, Integer watcherId) {
    
        return (Boolean) getSession().getNamedQuery("Watcher_isIssueWatchedByUser")
                                     .setInteger("issueId", issueId)
                                     .setInteger("userId", watcherId)
                                     .uniqueResult();
    }
}
