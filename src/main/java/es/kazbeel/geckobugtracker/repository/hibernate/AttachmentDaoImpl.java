package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.Attachment;
import es.kazbeel.geckobugtracker.repository.AttachmentDao;


@Repository
public class AttachmentDaoImpl extends BaseDaoImpl<Attachment> implements AttachmentDao {
    
    @Override
    public Attachment findById (Integer attachmentId) {
    
        return (Attachment) getSession().get(Attachment.class, attachmentId);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Attachment> findAllFromIssue (Integer issueId) {
    
        return getSession().getNamedQuery("Attachment_findAllFromIssue").setInteger("issueId", issueId).list();
    }
}
