package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.Component;
import es.kazbeel.geckobugtracker.repository.ComponentDao;


@Repository
public class ComponentDaoImpl extends BaseDaoImpl<Component> implements ComponentDao {
    
    @Override
    public Component findById (Long componentId) {
    
        return (Component) getSession().get(Component.class, componentId);
    }
    
    @Override
    public Component findByName (String name) {
    
        return (Component) getSession().getNamedQuery("Component.findByName").setString("name", name).uniqueResult();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Component> findByProject (Long projectId) {
    
        return getSession().getNamedQuery("Component.findByProjectId").setLong("pid", projectId).list();
    }
}
