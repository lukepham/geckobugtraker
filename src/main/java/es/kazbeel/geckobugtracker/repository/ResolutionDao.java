package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.Resolution;


public interface ResolutionDao extends BaseDao<Resolution> {
    
    public List<Resolution> findAll ();
    
    public List<Resolution> findAllEnabled ();

    public Resolution findById (Integer id);

    public void increaseOrder (Integer id);

    public void decreaseOrder (Integer id);
}
