package es.kazbeel.geckobugtracker.repository;


import es.kazbeel.geckobugtracker.model.LoginInfo;


public interface LoginInfoDao extends BaseDao<LoginInfo> {
    
    public LoginInfo findByUsername (String username);
    
    public void resetFailAttempts (String username);
}
