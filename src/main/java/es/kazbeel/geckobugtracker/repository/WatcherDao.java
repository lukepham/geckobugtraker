package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.Watcher;


public interface WatcherDao extends BaseDao<Watcher> {
    
    public List<Watcher> findByUser (Integer userId, int offset, int size);
    
    public List<Watcher> findByIssue (Integer issueId);
    
    public Long countByUser (Integer userId);
    
    public Long countByIssue (Integer issueId);

    public Boolean isIssueWatchedByUser (Integer issueId, Integer watcherId);
}
