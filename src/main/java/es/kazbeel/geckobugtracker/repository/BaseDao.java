package es.kazbeel.geckobugtracker.repository;

import java.io.Serializable;


public interface BaseDao<T extends Serializable> {
    
    public void save (T entity);
    
    public void saveOrUpdate (T entity);
    
    public void delete (T entity);
    
    public void refresh (T entity);
}
