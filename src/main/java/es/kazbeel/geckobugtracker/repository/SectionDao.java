package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.Section;


public interface SectionDao extends BaseDao<Section> {
    
    public List<Section> findAll ();
    
    public Section findById (Long id);

    public Section findByName (String name);
    
    public void increaseSortValue (Long id);
    
    public void decreaseSortValue (Long id);
}
