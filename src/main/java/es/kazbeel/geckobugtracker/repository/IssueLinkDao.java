package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.IssueLink;


public interface IssueLinkDao extends BaseDao<IssueLink> {
    
    public IssueLink findById (Integer id);

    public List<IssueLink> findAllWithSource (Integer sourceId);
    
    public List<IssueLink> findAllWithDestination (Integer destinationId);

    public List<IssueLink> findAllRelatedToIssue (Integer issueId);
}
