package es.kazbeel.geckobugtracker.web.controller.admin;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.kazbeel.geckobugtracker.model.Tag;
import es.kazbeel.geckobugtracker.service.IssueService;


@Controller
public class TagController {
    
    private static final Logger LOG = LoggerFactory.getLogger(TagController.class);
    
    @Autowired
    IssueService issueService;
    
    
    @RequestMapping(value = "/admin/tags/ViewTags", method = RequestMethod.GET)
    public ModelAndView viewTags () {
    
        LOG.debug("Showing List of Tags");
        
        ModelAndView mav = new ModelAndView("/admin/tags/ViewTags");
        mav.addObject("tags", issueService.getAllTags());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/tags/CreateTag", method = RequestMethod.GET)
    public ModelAndView createTagGet () {
    
        LOG.debug("Showing Create Tag View");
        
        ModelAndView mav = new ModelAndView("/admin/tags/CreateTag");
        mav.addObject("tag", new Tag());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/tags/CreateTag", method = RequestMethod.POST)
    public String createTagPost (@Valid @ModelAttribute("tag") Tag tag, BindingResult result) {
    
        LOG.debug("Creating new Tag");
        
        if (result.hasErrors() == true) {
            LOG.debug("The tag is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/tags/CreateTag";
        }
        
        try {
            issueService.saveTag(tag);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/tags/CreateTag";
        }
        
        return "redirect:/admin/tags/ViewTags";
    }
    
    @RequestMapping(value = "/admin/tags/EditTag", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView editTagGet (@RequestParam("id") Integer tagId) {
    
        LOG.debug("Showing Edit Tag View (ID = {})", tagId);
        
        ModelAndView mav = new ModelAndView("/admin/tags/EditTag");
        mav.addObject("tag", issueService.getTagById(tagId));
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/tags/EditTag", params = {"id"}, method = RequestMethod.POST)
    public String editTagPost (@RequestParam("id") Integer tagId,
                               @Valid @ModelAttribute("tag") Tag tag,
                               BindingResult result) {
    
        LOG.debug("Updating Tag (ID = {})", tagId);
        
        if (result.hasErrors() == true) {
            LOG.debug("The tag is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/tags/EditTag";
        }
        
        try {
            issueService.saveTag(tag);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/tags/EditTag";
        }
        
        return "redirect:/admin/tags/ViewTags";
    }
    
    @RequestMapping(value = "/admin/tags/DeleteTag", params = {"id"}, method = RequestMethod.GET)
    public String deleteTag (@RequestParam("id") Integer tagId,
                             RedirectAttributes redirectAttributes) {
    
        LOG.debug("Deleting Tag (ID = {})", tagId);
        
        try {
            Tag tag = issueService.getTagById(tagId);
            
            issueService.deleteTag(tag);
            redirectAttributes.addFlashAttribute("msgSuccess", "Tag deleted successfully.");
            
            LOG.debug("Tag deleted successfully");
        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("msgError",
                                                 "Tag cannot be deleted.");
            LOG.debug("Status cannot be deleted because it's used by some issues");
        } catch (IllegalArgumentException ex) {
            redirectAttributes.addFlashAttribute("msgError", "Tag does not exist.");
            
            LOG.debug("Tag does not exist");
        }
        
        return "redirect:/admin/tags/ViewTags";
    }
}
