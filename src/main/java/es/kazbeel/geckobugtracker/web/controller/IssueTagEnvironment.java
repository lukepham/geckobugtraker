package es.kazbeel.geckobugtracker.web.controller;


import org.springframework.stereotype.Component;

import es.kazbeel.geckobugtracker.model.Issue;
import es.kazbeel.geckobugtracker.model.Project;


@Component
public class IssueTagEnvironment {
    
    private Issue issue;
    
    private String environmentStr;
    
    private String tagStr;
    
    private Project project;
    
    public IssueTagEnvironment () {
    
        issue = new Issue();
//        project = new Project();
        environmentStr = "";
        tagStr = "";
    }
    
    public Issue getIssue () {
    
        return issue;
    }
    
    public void setIssue (Issue issue) {
    
        this.issue = issue;
    }
    
    public String getEnvironmentStr () {
    
        return environmentStr;
    }
    
    public void setEnvironmentStr (String environmentStr) {
    
        this.environmentStr = environmentStr;
    }
    
    public String getTagStr () {
    
        return tagStr;
    }
    
    public void setTagStr (String tagStr) {
    
        this.tagStr = tagStr;
    }

    
    public Project getProject () {
    
        return project;
    }

    
    public void setProject (Project project) {
    
        this.project = project;
    }
}
