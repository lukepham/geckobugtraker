package es.kazbeel.geckobugtracker.web.controller.admin;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.kazbeel.geckobugtracker.model.Project;
import es.kazbeel.geckobugtracker.model.Section;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.service.ProjectService;
import es.kazbeel.geckobugtracker.service.UserGroupService;
import es.kazbeel.geckobugtracker.web.controller.exception.ResourceNotFoundException;
import es.kazbeel.geckobugtracker.web.entityeditors.SectionEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.UserEditor;


@Controller
public class ProjectController {
    
    private static final Logger LOG = LoggerFactory.getLogger(ProjectController.class);
    
    @Autowired
    private ProjectService projectService;
    
    @Autowired
    private UserGroupService userGroupService;
    
    
    @InitBinder
    public void initBinder (WebDataBinder binder) {
    
        binder.registerCustomEditor(Section.class, new SectionEditor(projectService));
        binder.registerCustomEditor(User.class, new UserEditor(userGroupService));
    }
    
    @RequestMapping(value = "/admin/projects/ViewProjects", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView viewProjectsGet (@ModelAttribute("sectionForm") SectionForm sectionForm,
                                         RedirectAttributes redirectAttributes, ModelMap model) {
    
        ModelAndView mav = new ModelAndView("/admin/projects/ViewProjects");
        List<Section> sections = new ArrayList<Section>(projectService.getAllSections());
        
        model.clear();
        
        if (sections.isEmpty() == true) {
            mav.setViewName("redirect:/admin/sections/ViewSections");
            
            redirectAttributes.addFlashAttribute("msgError", "First create one Section.");
            
            return mav;
        }
        
        mav.addObject("sections", sections);
        
        Collection<Project> projects;
        Section currentSection = sectionForm.getSection();
        
        if (currentSection == null) {
            LOG.debug("Showing List of all Projects");
            
            projects = projectService.getAllProjects();
        } else {
            LOG.debug("Showing List of Projects belonging to {}", currentSection.getName());
            
            projects = projectService.getProjectsBySection(currentSection);
        }
        
        mav.addObject("projects", projects);
        mav.addObject("section", currentSection);
        mav.addObject("sectionForm", sectionForm);
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/projects/CreateProjectForm", method = RequestMethod.POST)
    public ModelAndView createProjectForm (@ModelAttribute("sectionId") Long sectionId) {
    
        ModelAndView mav = new ModelAndView("/admin/projects/CreateProject");

        if (sectionId == -1) {
            LOG.debug("Showing Create Project view");
            
            mav.addObject("sections", projectService.getAllSections());
        } else {
            LOG.debug("Showing Create Project view (preassigned Section ID={})", sectionId);
            
            mav.addObject("section", projectService.getSectionById(sectionId));
        }
        
        mav.addObject("users", userGroupService.getAllEnabledUsers());
        mav.addObject("project", new Project());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/projects/CreateProject", method = RequestMethod.POST)
    public String createProjectPost (@Valid @ModelAttribute("project") Project project,
                                     BindingResult result,
                                     RedirectAttributes redirectAttributes) {
    
        LOG.debug("Creating new Project");
        
        if (result.hasErrors() == true) {
            LOG.debug("The project is not valid: {} -> {}",
                      result.getFieldError().getField(),
                      result.getFieldError().getDefaultMessage());
            
            return "/admin/projects/CreateProject";
        }
        
        try {
            projectService.saveProjectWithChildComponent(project);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/projects/CreateProject";
        }
        
        redirectAttributes.addAttribute("id", project.getId());
        return "redirect:/admin/projects/DetailsProject";
    }
    
    @RequestMapping(value = "/admin/projects/EditProject", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView editProjectGet (@RequestParam("id") Long projectId) {
    
        LOG.debug("Showing Edit Project view (ID = {})", projectId);
        
        ModelAndView mav = new ModelAndView("/admin/projects/EditProject");
        mav.addObject("users", userGroupService.getAllEnabledUsers());
        mav.addObject("sections", projectService.getAllSections());
        mav.addObject("project", projectService.getProjectById(projectId));
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/projects/EditProject", params = {"id"}, method = RequestMethod.POST)
    public String editProjectPost (@RequestParam("id") Long projectId,
                                   @Valid @ModelAttribute("project") Project project,
                                   BindingResult result,
                                   RedirectAttributes redirectAttributes) {
    
        LOG.debug("Updating Project (ID = {})", projectId);
        
        if (result.hasErrors() == true) {
            LOG.debug("The project is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/projects/EditProject";
        }
        
        try {
            projectService.saveProject(project);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/projects/EditProject";
        }
        
        redirectAttributes.addAttribute("id", projectId);
        
        return "redirect:/admin/projects/DetailsProject";
    }
    
    @RequestMapping(value = "/admin/projects/DeleteProject", params = {"id"}, method = RequestMethod.GET)
    public String deleteProject (@RequestParam("id") Long projectId, RedirectAttributes redirectAttributes) {
    
        LOG.debug("Deleting Project (ID = {})", projectId);
        
        try {
            projectService.deleteProject(projectService.getProjectById(projectId));
            redirectAttributes.addFlashAttribute("msgSuccess", "Project deleted successfully.");
            
            LOG.debug("Section deleted successfully");
        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("msgError",
                                                 "Project cannot be deleted because it interacts with some components.");
            LOG.debug("Project cannot be deleted because it interacts with some components");
        } catch (IllegalArgumentException ex) {
            redirectAttributes.addFlashAttribute("msgError", "Project does not exist.");
            LOG.debug("Project does not exist");
        }
        
        return "redirect:/admin/projects/ViewProjects";
    }
    
    @RequestMapping(value = "/admin/projects/DetailsProject", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView detailsProject (@RequestParam("id") Long projectId) {
    
        LOG.debug("Showing Deatils of Project (ID = {})", projectId);
        
        ModelAndView mav = new ModelAndView("/admin/projects/DetailsProject");
        
        Project project = projectService.getProjectByIdWithSharedComponentsAndVersions(projectId);
        
        if (project == null) {
            LOG.debug("Requested Project does not exist");
            
            throw new ResourceNotFoundException();
        }
        
        mav.addObject("project", project);
        
        return mav;
    }
}
