package es.kazbeel.geckobugtracker.web.controller.admin;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.kazbeel.geckobugtracker.model.Project;
import es.kazbeel.geckobugtracker.model.ProjectVersion;
import es.kazbeel.geckobugtracker.service.ProjectService;
import es.kazbeel.geckobugtracker.web.controller.exception.ResourceNotFoundException;
import es.kazbeel.geckobugtracker.web.entityeditors.ProjectEditor;


@Controller
public class ProjectVersionController {
    
    private static final Logger LOG = LoggerFactory.getLogger(ProjectVersionController.class);
    
    @Autowired
    private ProjectService projectService;
    
    
    @InitBinder
    public void initBinder (WebDataBinder binder) {
    
        binder.registerCustomEditor(Project.class, new ProjectEditor(projectService));
    }
    
    @RequestMapping(value = "/admin/projectversions/CreateProjectVersionForm", method = RequestMethod.POST)
    public ModelAndView createProjectVersionForm (@ModelAttribute("projectId") Long projectId) {
    
        LOG.debug("Showing Create Project Version view (preassigned Project = {})", projectId);
        
        ModelAndView mav = new ModelAndView("/admin/projectversions/CreateProjectVersion");
        mav.addObject("project", projectService.getProjectById(projectId));
        mav.addObject("projectVersion", new ProjectVersion());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/projectversions/CreateProjectVersion", method = RequestMethod.POST)
    public String createProjectVersionPost (@Valid @ModelAttribute("projectVersion") ProjectVersion projectVersion,
                                            BindingResult result,
                                            RedirectAttributes redirectAttributes) {
    
        LOG.debug("Creating new Project Version");
        
        if (result.hasErrors() == true) {
            LOG.debug("The Project Version is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/projectversions/CreateProjectVersionForm";
        }
        
        try {
            projectService.saveProjectVersion(projectVersion);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/projectversions/CreateProjectVersionForm";
        }
        
        redirectAttributes.addAttribute("id", projectVersion.getProject().getId());
        
        return "redirect:/admin/projects/DetailsProject";
    }
    
    @RequestMapping(value = "/admin/projectversions/EditProjectVersion", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView editProjectVersionGet (@RequestParam("id") Long projectVersionId) {
    
        LOG.debug("Showing Edit Project Version view (ID = {})", projectVersionId);
        
        ModelAndView mav = new ModelAndView("/admin/projectversions/EditProjectVersion");
        mav.addObject("projectVersion", projectService.getProjectVersionById(projectVersionId));
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/projectversions/EditProjectVersion", params = {"id"}, method = RequestMethod.POST)
    public String editProjectVersionPost (@RequestParam("id") Long projectVersionId,
                                     @Valid @ModelAttribute("projectVersion") ProjectVersion projectVersion,
                                     BindingResult result,
                                     RedirectAttributes redirectAttributes) {
    
        LOG.debug("Updating Project Version (ID = {})", projectVersionId);
        
        if (result.hasErrors() == true) {
            LOG.debug("The \"{}\" is not valid: {}", result.getFieldError().getField(), result.getFieldError()
                                                                                              .getDefaultMessage());
            
            return "/admin/projectversions/EditProjectVersion";
        }
        
        try {
            projectService.saveProjectVersion(projectVersion);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/projectversions/EditProjectVersion";
        }
        
        redirectAttributes.addAttribute("id", projectVersion.getId());
        
        return "redirect:/admin/projectversions/DetailsProjectVersion";
    }
    
    @RequestMapping(value = "/admin/projectversions/DeleteProjectVersion", params = {"id"}, method = RequestMethod.GET)
    public String deleteProjectVersion (@RequestParam("id") Long projectVersionId, RedirectAttributes redirectAttributes) {
    
        Long projectId;
        
        LOG.debug("Deleting Project Version (ID = {})", projectVersionId);
        
        try {
            ProjectVersion projectVersion = projectService.getProjectVersionById(projectVersionId);
            
            projectService.deleteProjectVersion(projectVersion);
            
            projectId = projectVersion.getProject().getId();
            
            redirectAttributes.addFlashAttribute("msgSuccess", "Project Version deleted successfully.");
            redirectAttributes.addAttribute("id", projectId);
            
            LOG.debug("Project Version deleted successfully");

            return "redirect:/admin/projects/DetailsProject";
        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("msgError",
                                                 "Project Version cannot be deleted because it interacts with some issues.");
            LOG.debug("Project Version cannot be deleted because it interacts with some issues");
        } catch (IllegalArgumentException ex) {
            redirectAttributes.addFlashAttribute("msgError", "Project Version does not exist.");
            LOG.debug("Project Version cannot be deleted because it interacts with some issues");
        }
        
        return "redirect:/admin/projects/ViewProjects";
    }
    
    @RequestMapping(value = "/admin/projectversions/DetailsProjectVersion", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView detailsProjectVersion (@RequestParam("id") Long projectVersionId) {
    
        LOG.debug("Showing Details of Project Version (ID = {})", projectVersionId);
        
        ModelAndView mav = new ModelAndView("/admin/projectversions/DetailsProjectVersion");
        
        ProjectVersion projectVersion = projectService.getProjectVersionById(projectVersionId);
        
        if (projectVersion == null) {
            LOG.debug("Requested Project Version does not exist");
            
            throw new ResourceNotFoundException();
        }
        
        mav.addObject("projectVersion", projectVersion);
        
        return mav;
    }
}
