package es.kazbeel.geckobugtracker.web.controller.admin;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.kazbeel.geckobugtracker.model.IssueLinkType;
import es.kazbeel.geckobugtracker.service.IssueService;


@Controller
public class IssueLinkTypeController {
    
    private static final Logger LOG = LoggerFactory.getLogger(IssueLinkTypeController.class);
    
    @Autowired
    IssueService issueService;
    
    
    @RequestMapping(value = "/admin/issuelinktypes/ViewIssueLinkTypes", method = RequestMethod.GET)
    public ModelAndView viewIssueLinkTypes () {
    
        LOG.debug("Showing List of Issue Link Types View");
        
        ModelAndView mav = new ModelAndView("/admin/issuelinktypes/ViewIssueLinkTypes");
        mav.addObject("issueLinkTypes", issueService.getAllIssueLinkTypes());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/issuelinktypes/CreateIssueLinkType", method = RequestMethod.GET)
    public ModelAndView createIssueLinkTypeGet () {
    
        LOG.debug("Showing Create Issue Link Type View");
        
        ModelAndView mav = new ModelAndView("/admin/issuelinktypes/CreateIssueLinkType");
        mav.addObject("issueLinkType", new IssueLinkType());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/issuelinktypes/CreateIssueLinkType", method = RequestMethod.POST)
    public String createIssueLinkTypePost (@Valid @ModelAttribute("issueLinkType") IssueLinkType issueLinkType,
                                            BindingResult result) {
    
        LOG.debug("Creating new Issue Link Type");
        
        if (result.hasErrors() == true) {
            LOG.debug("The issue link type is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/issuelinktypes/CreateIssueLinkType";
        }
        
        try {
            issueService.saveIssueLinkType(issueLinkType);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/issuelinktypes/CreateIssueLinkType";
        }
        
        
        return "redirect:/admin/issuelinktypes/ViewIssueLinkTypes";
    }
    
    @RequestMapping(value = "/admin/issuelinktypes/EditIssueLinkType", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView editIssueLinkTypeGet (@RequestParam("id") Integer issueLinkTypeId) {
    
        LOG.debug("Showing Edit Issue Link Type page (ID = {})", issueLinkTypeId);
        
        ModelAndView mav = new ModelAndView("/admin/issuelinktypes/EditIssueLinkType");
        mav.addObject("issueLinkType", issueService.getIssueLinkTypeById(issueLinkTypeId));
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/issuelinktypes/EditIssueLinkType", params = {"id"}, method = RequestMethod.POST)
    public String editIssueLinkTypePost (@RequestParam("id") Integer issueLinkTypeId,
                                         @Valid @ModelAttribute("issueLinkType") IssueLinkType issueLinkType,
                                         BindingResult result) {
    
        LOG.debug("Updating Issue Link Type (ID = {})", issueLinkTypeId);
        
        if (result.hasErrors() == true) {
            LOG.debug("The issue link type is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/issuelinktypes/EditIssueLinkType";
        }
        
        try {
            issueService.saveIssueLinkType(issueLinkType);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/issuelinktypes/EditIssueLinkType";
        }
        
        return "redirect:/admin/issuelinktypes/ViewIssueLinkTypes";
    }
    
    @RequestMapping(value = "/admin/issuelinktypes/DeleteIssueLinkType", params = {"id"}, method = RequestMethod.GET)
    public String deleteIssueLinkType (@RequestParam("id") Integer issueLinkTypeId,
                                       RedirectAttributes redirectAttributes) {
    
        LOG.debug("Deleting Issue Link Type (ID = {})", issueLinkTypeId);
        
        try {
            IssueLinkType issueLinkType = issueService.getIssueLinkTypeById(issueLinkTypeId);
            
            issueService.deleteIssueLinkType(issueLinkType);
            
            redirectAttributes.addFlashAttribute("msgSuccess", "IssueLinkType deleted successfully.");
            
            LOG.debug("IssueLinkType deleted successfully");
        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("msgError",
                                                 "IssueLinkType cannot be deleted because it interacts with some issues.");
            
            LOG.debug("IssueLinkType cannot be deleted because it interacts with some issues");
        } catch (IllegalArgumentException ex) {
            redirectAttributes.addFlashAttribute("msgError", "IssueLinkType does not exist.");
            
            LOG.debug("IssueLinkType does not exist");
        }
        
        return "redirect:/admin/issuelinktypes/ViewIssueLinkTypes";
    }
}
