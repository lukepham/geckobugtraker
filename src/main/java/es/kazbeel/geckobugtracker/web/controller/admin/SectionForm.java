package es.kazbeel.geckobugtracker.web.controller.admin;


import es.kazbeel.geckobugtracker.model.Section;


public class SectionForm {
    
    Section section;
    
    
    public SectionForm () {
    
    }
    
    public Section getSection () {
    
        return section;
    }
    
    public void setSection (Section section) {
    
        this.section = section;
    }
}
