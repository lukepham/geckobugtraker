package es.kazbeel.geckobugtracker.web.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import es.kazbeel.geckobugtracker.model.Issue;
import es.kazbeel.geckobugtracker.model.Resolution;
import es.kazbeel.geckobugtracker.service.IssueService;
import es.kazbeel.geckobugtracker.service.UserGroupService;
import es.kazbeel.geckobugtracker.support.Page;
import es.kazbeel.geckobugtracker.support.PageRequest;
import es.kazbeel.geckobugtracker.web.utils.SecurityUtils;


@Controller
public class IssueController {
    
    private static final Logger LOG = LoggerFactory.getLogger(IssueController.class);
    
    @Autowired
    private IssueService issueService;
    
    @Autowired
    UserGroupService userGroupService;
    
    @RequestMapping(value = "/issues/AllIssues", method = RequestMethod.GET)
    public ModelAndView allIssues (@RequestParam(value = "page", required = false, defaultValue = "0") Integer page) {
    
        LOG.debug("Showing All Issues. Page = {}", page);
        
        PageRequest pageRequest = new PageRequest(page, 3);
        
        Page<Issue> issuesPage = issueService.getAllIssues(pageRequest);
        
        if (issuesPage.getTotalPages() < page) {
            LOG.debug("Total number of issue pages is less than required -> Refresing the current web page.");
            
            return new ModelAndView("redirect:AllIssues");
        }
        
        ModelAndView mav = new ModelAndView("issues/AllIssues");
        mav.addObject("loggedUser", SecurityUtils.getCurrentUser());
        mav.addObject("issues", issuesPage.getContent());
        mav.addObject("totalPages", issuesPage.getTotalPages());
        mav.addObject("isFirst", issuesPage.isFirst());
        mav.addObject("isLast", issuesPage.isLast());
        
        return mav;
    }
    
    @RequestMapping(value = "/issues/AssignedToMe", method = RequestMethod.GET)
    public ModelAndView assignedToMeAndUnresolvedIssuesGet (@RequestParam(value = "page", required = false, defaultValue = "0") Integer page) {
    
        LOG.debug("Showing Issues Assigned to Me (Unresolved). Page = {}", page);
        
        PageRequest pageRequest = new PageRequest(page, 3);
        
        Resolution unresolved = new Resolution();
        unresolved.setId(1);
        
        Page<Issue> issuesPage = issueService.getIssuesByAssigneeAndResolution(SecurityUtils.getCurrentUser(),
                                                                               unresolved,
                                                                               pageRequest);
        
        if (issuesPage.getTotalPages() < page) {
            LOG.debug("Total number of issue pages is less than required -> Refresing the current web page.");
            
            return new ModelAndView("redirect:AssignedToMe");
        }
        
        ModelAndView mav = new ModelAndView("issues/AssignedToMe");
        
        mav.addObject("issues", issuesPage.getContent());
        mav.addObject("loggedUser", SecurityUtils.getCurrentUser());
        mav.addObject("totalPages", issuesPage.getTotalPages());
        mav.addObject("isFirst", issuesPage.isFirst());
        mav.addObject("isLast", issuesPage.isLast());
        
        return mav;
    }
    
    @RequestMapping(value = "/issues/ReportedByMe", method = RequestMethod.GET)
    public ModelAndView reportedByMeGet (@RequestParam(value = "page", required = false, defaultValue = "0") Integer page) {
    
        LOG.debug("Showing Issues Reported by Me. Page = {}", page);
        
        PageRequest pageRequest = new PageRequest(page, 3);
        
        Page<Issue> issuesPage = issueService.getIssuesByReporter(SecurityUtils.getCurrentUser(), pageRequest);
        
        if (issuesPage.getTotalPages() < page) {
            LOG.debug("Total number of issue pages is less than required -> Refresing the current web page.");
            
            return new ModelAndView("redirect:ReportedByMe");
        }
        
        ModelAndView mav = new ModelAndView("issues/ReportedByMe");
        
        mav.addObject("issues", issuesPage.getContent());
        mav.addObject("loggedUser", SecurityUtils.getCurrentUser());
        mav.addObject("totalPages", issuesPage.getTotalPages());
        mav.addObject("isFirst", issuesPage.isFirst());
        mav.addObject("isLast", issuesPage.isLast());
        
        return mav;
    }
    
    @RequestMapping(value = "/issues/VotedByMe", method = RequestMethod.GET)
    public ModelAndView votedByMe (@RequestParam(value = "page", required = false, defaultValue = "0") Integer page) {
    
        LOG.debug("Showing Issues Voted by Me. Page = {}", page);
        
        PageRequest pageRequest = new PageRequest(page, 3);
        
        Page<Issue> issuesPage = issueService.getIssuesVotedByUser(SecurityUtils.getCurrentUser(), pageRequest);
        
        if (issuesPage.getTotalPages() < page) {
            LOG.debug("Total number of issue pages is less than required -> Refresing the current web page.");
            
            return new ModelAndView("redirect:VotedByMe");
        }
        
        ModelAndView mav = new ModelAndView("issues/VotedByMe");
        
        mav.addObject("issues", issuesPage.getContent());
        mav.addObject("loggedUser", SecurityUtils.getCurrentUser());
        mav.addObject("totalPages", issuesPage.getTotalPages());
        mav.addObject("isFirst", issuesPage.isFirst());
        mav.addObject("isLast", issuesPage.isLast());
        
        return mav;
    }
    
    @RequestMapping(value = "/issues/WatchedByMe", method = RequestMethod.GET)
    public ModelAndView watchedByMe (@RequestParam(value = "page", required = false, defaultValue = "0") Integer page) {
    
        LOG.debug("Showing Issues Watched by Me. Page = {}", page);
        
        PageRequest pageRequest = new PageRequest(page, 3);
        
        Page<Issue> issuesPage = issueService.getIssuesWatchedByUser(SecurityUtils.getCurrentUser(), pageRequest);
        
        if (issuesPage.getTotalPages() < page) {
            LOG.debug("Total number of issue pages is less than required -> Refresing the current web page.");
            
            return new ModelAndView("redirect:WatchedByMe");
        }
        
        ModelAndView mav = new ModelAndView("issues/WatchedByMe");
        
        mav.addObject("issues", issuesPage.getContent());
        mav.addObject("loggedUser", SecurityUtils.getCurrentUser());
        mav.addObject("totalPages", issuesPage.getTotalPages());
        mav.addObject("isFirst", issuesPage.isFirst());
        mav.addObject("isLast", issuesPage.isLast());
        
        return mav;
    }
}
