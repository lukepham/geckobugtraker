package es.kazbeel.geckobugtracker.web.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.kazbeel.geckobugtracker.model.Issue;
import es.kazbeel.geckobugtracker.model.Resolution;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.service.IssueService;
import es.kazbeel.geckobugtracker.service.ProjectService;
import es.kazbeel.geckobugtracker.support.Page;
import es.kazbeel.geckobugtracker.support.PageRequest;
import es.kazbeel.geckobugtracker.web.utils.SecurityUtils;


@Controller
public class DashboardController {
    
    private static final Logger LOG = LoggerFactory.getLogger(DashboardController.class);
    
    @Autowired
    private IssueService issueService;
    
    @Autowired
    private ProjectService projectService;
    
    
    @RequestMapping(value = "/SearchIssue", method = RequestMethod.POST)
    public String searchIssue (@ModelAttribute("searchIssue") String issueId,
                               ModelMap model,
                               RedirectAttributes redirectAttributes) {
    
        // Remove "searchIssue" parameter from the Model
        model.clear();
        
        try {
            LOG.debug("Searching for issue #{}", issueId);
            
            if (issueService.getIssueById(Integer.parseInt(issueId)) != null) {
                LOG.debug("Issue #{} found", issueId);
                
                return "redirect:/issues/" + issueId;
            }
        } catch (NumberFormatException ex) {
            LOG.debug("Issue #{} is malformed", issueId);
            
            redirectAttributes.addFlashAttribute("formatError", "Issue ID is malformed.");
            return "redirect:/Dashboard";
        }
        
        redirectAttributes.addFlashAttribute("notFound", "Issue ID not found.");
        return "redirect:/Dashboard";
    }
    
    @RequestMapping(value = "/Dashboard", method = RequestMethod.GET)
    public ModelAndView dashboardGet () {//TESTME
    
        LOG.debug("Showing Dashboard page");
        
        User loggedUser = SecurityUtils.getCurrentUser();
        
        PageRequest pageRequest = new PageRequest(0, 5);
        
        Resolution unresolved = new Resolution();
        unresolved.setId(1);
        
        ModelAndView mav = new ModelAndView("Dashboard");
        
        mav.addObject("sections", projectService.getAllSections());

        Page<Issue> assignedToMeUnresolved = issueService.getIssuesByAssigneeAndResolution(loggedUser,
                                                                                           unresolved,
                                                                                           pageRequest);
        mav.addObject("issuesAssignedToMe", assignedToMeUnresolved.getContent());
        mav.addObject("totalAssignedToMe", assignedToMeUnresolved.getTotalElements());
        
        Page<Issue> reportedByMe = issueService.getIssuesByReporter(loggedUser, pageRequest);
        mav.addObject("issuesReportedByMe", reportedByMe.getContent());
        mav.addObject("totalReportedByMe", reportedByMe.getTotalElements());
        
        Page<Issue> votedByMe = issueService.getIssuesVotedByUser(loggedUser, pageRequest);
        mav.addObject("issuesVotedByMe", votedByMe.getContent());
        mav.addObject("totalVoted", votedByMe.getTotalElements());
        
        Page<Issue> watchedByMe = issueService.getIssuesWatchedByUser(loggedUser, pageRequest);
        mav.addObject("issuesWatchedByMe", watchedByMe.getContent());
        mav.addObject("totalWatched", watchedByMe.getTotalElements());
        
        return mav;
    }
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String root () {
    
        LOG.debug("Redirectiong from root to Dashboard");
        
        return "redirect:/Dashboard";
    }
}
