package es.kazbeel.geckobugtracker.web.controller.admin;


import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.kazbeel.geckobugtracker.model.Group;
import es.kazbeel.geckobugtracker.service.UserGroupService;
import es.kazbeel.geckobugtracker.support.Page;
import es.kazbeel.geckobugtracker.support.PageRequest;
import es.kazbeel.geckobugtracker.web.entityeditors.UserListEditor;


@Controller
@SessionAttributes("userList")
public class GroupController {
    
    private static final Logger LOG = LoggerFactory.getLogger(GroupController.class);
    
    @Autowired
    private UserGroupService userGroupService;
    
    
    @ModelAttribute("userList")
    public void populate (Model model) {
    
        model.addAttribute("userList", userGroupService.getAllEnabledUsers());
    }
    
    @InitBinder
    public void initBinder (WebDataBinder binder) {
    
        binder.registerCustomEditor(Set.class, new UserListEditor(userGroupService));
    }
    
    @RequestMapping(value = "/admin/groups/ViewGroups", method = RequestMethod.GET)
    public ModelAndView viewGroups (@RequestParam(value = "page", required = false, defaultValue = "0") Integer page) {
    
        LOG.debug("Showing List of Groups (page = {})", page);
        
        PageRequest pageRequest = new PageRequest(page, 5);
        Page<Group> groupsPage = userGroupService.getAllGroups(pageRequest);
        
        if (groupsPage.getTotalPages() < page) {
            LOG.debug("Requesting to show page {} out of {}", page, groupsPage.getTotalPages());
            
            return new ModelAndView("redirect:/admin/groups/ViewGroups");
        }
        
        ModelAndView mav = new ModelAndView("/admin/groups/ViewGroups");
        mav.addObject("groups", groupsPage.getContent());
        mav.addObject("totalPages", groupsPage.getTotalPages());
        mav.addObject("isFirst", groupsPage.isFirst());
        mav.addObject("isLast", groupsPage.isLast());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/groups/CreateGroup", method = RequestMethod.GET)
    public ModelAndView createGroupGet () {
    
        LOG.debug("Showing Create Group View");
        
        ModelAndView mav = new ModelAndView("/admin/groups/CreateGroup");
        mav.addObject("group", new Group());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/groups/CreateGroup", method = RequestMethod.POST)
    public String createGroupPost (@Valid @ModelAttribute("group") Group group,
                                   BindingResult result,
                                   SessionStatus status) {
    
        LOG.debug("Creating new Group");
        
        if (result.hasErrors() == true) {
            LOG.debug("The group is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/groups/CreateGroup";
        }
        
        try {
            userGroupService.saveGroup(group);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/groups/CreateGroup";
        }
        
        status.setComplete();

        return "redirect:/admin/groups/ViewGroups";
    }
    
    @RequestMapping(value = "/admin/groups/EditGroup", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView editGroupGet (@RequestParam("id") Integer groupId) {
    
        LOG.debug("Showing Edit Group page (ID = {})", groupId);
        
        ModelAndView mav = new ModelAndView("/admin/groups/EditGroup");
        mav.addObject("group", userGroupService.getGroupById(groupId));
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/groups/EditGroup", params = {"id"}, method = RequestMethod.POST)
    public String editGroupPost (@RequestParam("id") Integer groupId,
                                 @Valid @ModelAttribute("group") Group group,
                                 BindingResult result,
                                 SessionStatus status) {
    
        LOG.debug("Updating Group (ID = {})", groupId);
        
        if (result.hasErrors() == true) {
            LOG.debug("The group is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/groups/EditGroup";
        }
        
        try {
            userGroupService.saveGroup(group);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/groups/EditGroup";
        }

        status.setComplete();

        return "redirect:/admin/groups/ViewGroups";
    }
    
    @RequestMapping(value = "/admin/groups/DeleteGroup", params = {"id"}, method = RequestMethod.GET)
    public String deleteGroup (@RequestParam("id") Integer groupId, RedirectAttributes redirectAttributes) {
    
        LOG.debug("Deleting Group (ID = {})", groupId);
        
        try {
            Group group = userGroupService.getGroupById(groupId);
            
            userGroupService.deleteGroup(group);
            
            redirectAttributes.addFlashAttribute("msgSuccess", "Group deleted successfully.");
            
            LOG.debug("Group deleted successfully");
        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("msgError",
                                                 "Group cannot be deleted.");
            
            LOG.debug("Group cannot be deleted");
        } catch (IllegalArgumentException ex) {
            redirectAttributes.addFlashAttribute("msgError", "Group does not exist.");
            
            LOG.debug("Group does not exist");
        }
        
        return "redirect:/admin/groups/ViewGroups";
    }
}
