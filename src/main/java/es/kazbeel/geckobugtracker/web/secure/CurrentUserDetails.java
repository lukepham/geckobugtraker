package es.kazbeel.geckobugtracker.web.secure;


import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import es.kazbeel.geckobugtracker.model.User;


public class CurrentUserDetails extends org.springframework.security.core.userdetails.User
{
    
    private static final long serialVersionUID = 2651502779738652991L;
    
    private final User currentUser;
    
    
    public CurrentUserDetails (String username,
                               String password,
                               boolean enabled,
                               boolean accountNonExpired,
                               boolean credentialsNonExpired,
                               boolean accountNonLocked,
                               Collection<? extends GrantedAuthority> authorities,
                               User currentUser) {
    
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        
        this.currentUser = currentUser;
    }
    
    public CurrentUserDetails (String username,
                               String password,
                               Collection<? extends GrantedAuthority> authorities,
                               User currentUser) {
    
        super(username, password, authorities);
        
        this.currentUser = currentUser;
    }
    
    public User getCurrentUser () {
    
        return currentUser;
    }
}
