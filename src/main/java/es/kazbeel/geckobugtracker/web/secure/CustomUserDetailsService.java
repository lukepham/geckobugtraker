package es.kazbeel.geckobugtracker.web.secure;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.kazbeel.geckobugtracker.model.LoginInfo;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.repository.LoginInfoDao;
import es.kazbeel.geckobugtracker.repository.UserDao;


@Service
public class CustomUserDetailsService implements UserDetailsService {
    
    private static final Logger LOG = LoggerFactory.getLogger(CustomUserDetailsService.class);
    
    @Autowired
    private UserDao userDao;
    
    @Autowired
    private LoginInfoDao loginInfoDao;
    
    
    @Override
    @Transactional
    public UserDetails loadUserByUsername (String username) throws UsernameNotFoundException {
    
        LOG.debug("Loading user details (username = {})", username);
        
        User user = userDao.findByLoginName(username);
        
        if (user == null) {
            LOG.debug("User does not exist");
            
            throw new UsernameNotFoundException("Username " + username + " not found.");
        }
        
        LoginInfo loginInfo = loginInfoDao.findByUsername(username);
        
        if (loginInfo == null) {
            LOG.debug("First time the user logs in");
            
            loginInfo = new LoginInfo(username);
            
            loginInfoDao.save(loginInfo);
        }
        
        boolean enabled = user.isEnabled();
        boolean accountNonLocked = !loginInfo.isLocked();
        
        return new CurrentUserDetails(user.getLoginName(),
                                      user.getPassword(),
                                      enabled,
                                      true,
                                      true,
                                      accountNonLocked,
                                      getAuthorities(user.getLoginName()),
                                      user);
    }
    
    private Collection<? extends GrantedAuthority> getAuthorities (String loginName) {
    
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        
        if (loginName.equalsIgnoreCase("admin")) {
            authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        }
        
        LOG.debug("Roles assigned to user: {}", Arrays.toString(authorities.toArray()));
        
        return authorities;
    }
}
