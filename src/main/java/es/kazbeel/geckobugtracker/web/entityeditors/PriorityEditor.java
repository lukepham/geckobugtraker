package es.kazbeel.geckobugtracker.web.entityeditors;


import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.kazbeel.geckobugtracker.model.Priority;
import es.kazbeel.geckobugtracker.service.IssueService;


public class PriorityEditor extends PropertyEditorSupport {
    
    private static final Logger LOG = LoggerFactory.getLogger(PriorityEditor.class);
    
    private IssueService        issueService;
    
    
    public PriorityEditor (IssueService issueService) {
    
        this.issueService = issueService;
    }
    
    @Override
    public void setAsText (String id) {
    
        LOG.debug("Set ID=" + id + " as text");
        Priority priority = issueService.getPriorityById(Integer.parseInt(id));
        this.setValue(priority);
    }
    
    @Override
    public String getAsText () {
    
        Priority priority = (Priority) this.getValue();
        
        if (priority == null) {
            LOG.error("Previously set Priority has got a null type. Empty string returned");
            
            return "";
        }
        
        LOG.debug("Priority with ID=" + priority.getName() + " is returned");
        
        return priority.getName();
    }
    
}
