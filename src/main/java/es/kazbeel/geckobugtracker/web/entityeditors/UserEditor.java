package es.kazbeel.geckobugtracker.web.entityeditors;


import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.service.UserGroupService;


public class UserEditor extends PropertyEditorSupport {
    
    private static final Logger LOG = LoggerFactory.getLogger(UserEditor.class);
    
    private UserGroupService         userGroupService;
    
    
    public UserEditor (UserGroupService userGroupService) {
    
        this.userGroupService = userGroupService;
    }
    
    @Override
    public void setAsText (String id) {
    
        LOG.debug("Set ID=" + id + " as text");
        User user = userGroupService.getUserById(Integer.parseInt(id));
        this.setValue(user);
    }
    
    @Override
    public String getAsText () {
    
        User user = (User) this.getValue();
        
        if (user == null) {
            LOG.error("Previously set User has got a null type. Empty string returned");
            
            return "";
        }
        
        LOG.debug("User with ID=" + user.getLoginName() + " is returned");
        
        return user.getLoginName();
    }
}
