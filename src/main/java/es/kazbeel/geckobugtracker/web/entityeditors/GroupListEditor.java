package es.kazbeel.geckobugtracker.web.entityeditors;


import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;

import es.kazbeel.geckobugtracker.model.Group;
import es.kazbeel.geckobugtracker.service.UserGroupService;


public class GroupListEditor extends CustomCollectionEditor {
    
    private static final Logger LOG = LoggerFactory.getLogger(GroupListEditor.class);
    
    private UserGroupService userGroupService;
    
    
    public GroupListEditor (UserGroupService userGroupService) {
    
        super(Set.class, false);
        
        this.userGroupService = userGroupService;
    }
    
    @Override
    protected Object convertElement (Object element) {
    
        Group group = userGroupService.getGroupById(Integer.parseInt(element.toString()));
        
        if (group == null) {
            LOG.error("Previously set Group has got a null type. Empty string returned");
            
            return "";
        }
        
        LOG.debug("Group with name=" + group.getName() + " is returned");
        
        return group;
    }
}
