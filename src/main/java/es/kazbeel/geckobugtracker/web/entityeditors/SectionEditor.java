package es.kazbeel.geckobugtracker.web.entityeditors;


import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.kazbeel.geckobugtracker.model.Section;
import es.kazbeel.geckobugtracker.service.ProjectService;


public class SectionEditor extends PropertyEditorSupport {
    
    private static final Logger LOG = LoggerFactory.getLogger(SectionEditor.class);
    
    private ProjectService projectService;
    
    
    public SectionEditor (ProjectService projectService) {
    
        this.projectService = projectService;
    }
    
    @Override
    public void setAsText (String id) {
    
        if (id.equals("null")) {
            this.setValue(null);
        } else {
            Section section = projectService.getSectionById(Long.parseLong(id));
            
            if (section == null) {
                LOG.error("Section with ID=" + id + " does not exist");
            } else {
                this.setValue(section);
            }
        }
        
        LOG.debug("Set Section (ID=" + id + ") as text");
    }
    
    @Override
    public String getAsText () {
    
        Section section = (Section) this.getValue();
        
        if (section == null) {
            LOG.debug("No Section was set so \"null\" is returned");
            
            return "null";
        }
        
        LOG.debug("Get Section with ID=" + section.getId() + " is returned");
        
        return section.getId().toString();
    }
    
}
