package es.kazbeel.geckobugtracker.web.entityeditors;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;

import es.kazbeel.geckobugtracker.model.IssueLinkType;
import es.kazbeel.geckobugtracker.service.IssueService;


public class IssueLinkTypeListEditor extends CustomCollectionEditor {
    
    private static final Logger LOG = LoggerFactory.getLogger(IssueLinkTypeListEditor.class);
    
    private IssueService        issueService;
    
    
    public IssueLinkTypeListEditor (IssueService issueService) {
    
        super(List.class, false);
        
        this.issueService = issueService;
    }
    
    @Override
    protected Object convertElement (Object element) {
    
        IssueLinkType issueLinkType = issueService.getIssueLinkTypeById(Integer.parseInt(element.toString()));
        
        if (issueLinkType == null) {
            LOG.error("Previously set IssueLinkType has got a null type. Empty string returned");
            
            return "";
        }
        
        LOG.debug("IssueLinkType with ID=" + issueLinkType.getId() + " is returned");
        
        return issueLinkType;
    }
}
